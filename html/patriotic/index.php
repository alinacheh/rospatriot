<?php

/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 19.08.19
 * Time: 17:05
 */

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("РОСПАТРИОТЦЕНТР — центр гражданского и патриотического воспитания детей и молодежи");
?>
<!-- begin::prog -->
<div class="prog">
    <div class="container">
        <div class="prog__title page-title">Государственная программа</div>
        <div class="prog__text page-text">Государственная программа «Патриотическое воспитание граждан Российской Федерации на 2016-2020 годы»</div>
        <div class="prog__subtitle">Цели:</div>
        <div class="prog__flex">
            <div class="prog__box">
                <div class="prog__number">1</div>
                <div class="prog__info">Повышение гражданской ответственности за судьбу страны</div>
            </div>
            <div class="prog__box">
                <div class="prog__number">2</div>
                <div class="prog__info">Укрепление чувства сопричастности граждан к великой истории и культуре России
                </div>
            </div>
            <div class="prog__box">
                <div class="prog__number">3</div>
                <div class="prog__info">Обеспечение преемственности поколений россиян</div>
            </div>
            <div class="prog__box">
                <div class="prog__number">4</div>
                <div class="prog__info">Воспитание гражданина, любящего свою родину и семью, имеющего активную жизненную позицию
                </div>
            </div>
            <div class="prog__box">
                <div class="prog__number">5</div>
                <div class="prog__info">Консолидация общества для решения задач обеспечения национальной безопасности и устойчивого развития РФ</div>
            </div>
        </div>
    </div>
    <div class="prog-box">
        <div class="container">
            <div class="prog__subtitle">Исполнители государственной программы</div>
            <div class="prog-box__flex">
                <a href="https://minobrnauki.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us7.png" alt="">
                    </div>
                    <div class="prog-box__content">Министерство науки и высшего образования Российской Федерации</div>
                </a>
                <a href="https://edu.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us8.png" alt="">
                    </div>
                    <div class="prog-box__content">Министерство просвещения
                        <br>Российской Федерации</div>
                </a>
                <a href="http://mil.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us9.png" alt="">
                    </div>
                    <div class="prog-box__content">Министерство обороны
                        <br>Российской Федерации</div>
                </a>
                <a href="https://www.mkrf.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us1.png" alt="">
                    </div>
                    <div class="prog-box__content">Министерство культуры Российской Федерации</div>
                </a>
                <a href="https://www.minsport.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us5.png" alt="">
                    </div>
                    <div class="prog-box__content">Министерство спорта Российской Федерации</div>
                </a>
                <a href="https://www.mchs.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us2.png" alt="">
                    </div>
                    <div class="prog-box__content">МЧС России</div>
                </a>
                <a href="http://www.fapmc.ru" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us3.png" alt="">
                    </div>
                    <div class="prog-box__content">Федеральное агентство по печати и массовым коммуникациям</div>
                </a>
                <a href="http://rosleshoz.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us4.png" alt="">
                    </div>
                    <div class="prog-box__content">Федеральное агентство лесного хозяйства</div>
                </a>
                <a href="http://www.fish.gov.ru/" class="prog-box__box">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line"></use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="/local/templates/rospatriot/static/img/general/us6.png" alt="">
                    </div>
                    <div class="prog-box__content">Федеральное агентство по рыболовству</div>
                </a>
            </div>
            <div class="prog-box__double">
                <div class="prog-box__dox">
                    <div class="prog-box__desc">Координатор государственной программы
                    </div>
                    <div class="prog-box__img">
                        <img src="/local/templates/rospatriot/static/img/general/ros.png" alt="">
                    </div>
                    <div class="prog-box__txt">Федеральное агентство по делам молодёжи</div>
                </div>
                <div class="prog-box__dox">
                    <div class="prog-box__desc">Оператор государственной программы
                    </div>
                    <div class="prog-box__img">
                        <img src="/local/templates/rospatriot/static/img/general/ros2.png" alt="">
                    </div>
                    <div class="prog-box__txt">Федеральное государственное бюджетное учреждение «Российский центр гражданского и патриотического воспитания детей и молодёжи»</div>
                </div>
            </div>
        </div>
    </div>
    <div class="prog-uch">
        <div class="container">
            <div class="prog__subtitle">Участники государственной программы</div>
            <div class="prog-uch__flex">
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic1.png" alt="">
                    </div>
                    <div class="prog-uch__label">Школы</div>
                </div>
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic2.png" alt="">
                    </div>
                    <div class="prog-uch__label">Координационные советы
                        <br>по патриотическому воспитанию</div>
                </div>
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic3.png" alt="">
                    </div>
                    <div class="prog-uch__label">Вузы</div>
                </div>
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic6.png" alt="">
                    </div>
                    <div class="prog-uch__label">Патриотические клубы
                        <br>и объединения</div>
                </div>
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic5.png" alt="">
                    </div>
                    <div class="prog-uch__label">Воинские части (корабли)</div>
                </div>
                <div class="prog-uch__box">
                    <div class="prog-uch__icon">
                        <img src="/local/templates/rospatriot/static/img/general/ic4.png" alt="">
                    </div>
                    <div class="prog-uch__label">НКО, организации доп. образования, занимающиеся патриотическим воспитанием
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="prog-proj">
        <div class="container">
            <div class="prog__subtitle">Проекты и мероприятия</div>
            <? $GLOBALS["eventsFilterD"] = array(
                "PROPERTY_ATT_MAIN_VALUE" => "Да",
            );
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "projects_patriotic",
                array(
                    "ACTIVE_DATE_FORMAT" => "Y-m-d",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "USE_FILTER" => "Y",
                    "FILTER_NAME" => "eventsFilterD",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "12",
                    "IBLOCK_TYPE" => "developments",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "10",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Мероприятия",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "ATT_START",
                        1 => "ATT_END",
                        2 => "ATT_PLACE",
                        3 => "CATEGORY",
                        4 => "ATT_SECTION",
                        5 => "ATT_MAIN",
                        6 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "PROPERTY_ATT_START_VALUE",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => "projects_patriotic"
                ),
                false
            ); ?>
            <div class="prog__proj-footer">
                <a href="projects/" class="btn btn--base">загрузить ещё</a>
            </div>
            <div class="prog-proj__control">
                <div class="prog-proj__subtitle prog__subtitle">Контроль и управление государственными программами</div>
                <div class="prog-proj__white">
                    <div class="prog-proj__line">
                        Межведомственная комиссия по реализации государственной программы
                    </div>
                    <div class="prog-proj__text">Координация деятельности и организация взаимодействия федеральных органов исполнительной власти субъектов Российской Федерации, органов местного самоуправления и общественных объединений по вопросам реализации государственной
                        программы, принятие решений, необходимых для совершенствования этой деятельности.</div>
                </div>
            </div>
        </div>
    </div>



    <div class="prog-total">
        <div class="container">
            <div class="prog__subtitle">Финансовое обеспечение государственной программы</div>
            <div class="prog-total__flex">
                <div class="prog-total__control">
                    <div class="prog-total__row">
                        <div class="prog-total__total">Всего</div>
                        <div class="prog-total__desc">Всего
                            <br>по государственной
                            <br>программе</div>
                        <div class="prog-total__price">1 666 556,8 ₽</div>
                    </div>
                    <div class="prog-total__row">
                        <div class="prog-total__total">Федеральный
                            <br>бюджет</div>
                        <div class="prog-total__desc">Всего
                            <br>по государственной
                            <br>программе</div>
                        <div class="prog-total__price">1 74 056,8 ₽</div>
                    </div>
                    <div class="prog-total__row">
                        <div class="prog-total__total">внебюджетные
                            <br>источники</div>
                        <div class="prog-total__desc">Всего
                            <br>по государственной
                            <br>программе</div>
                        <div class="prog-total__price">92 500 ₽</div>
                    </div>
                </div>
                <div class="prog-total__slider">
                    <div class="prog-total__slide">
                        <div class="prog-total__row">
                            <div class="prog-total__date">2016</div>
                            <div class="prog-total__height">
                                <div class="prog-total__line" style="height: 70%"></div>
                            </div>
                            <div class="prog-total__pr">305 620,8 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="20"></div>
                                <div class="percircle__inn">2016</div>
                            </div>
                            <div class="prog-total__pr">290 120,8 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="20"></div>
                                <div class="percircle__inn">2016</div>
                            </div>
                            <div class="prog-total__pr">15 500 ₽</div>
                        </div>
                    </div>
                    <div class="prog-total__slide">
                        <div class="prog-total__row">
                            <div class="prog-total__date">2017</div>
                            <div class="prog-total__height">
                                <div class="prog-total__line" style="height: 80%"></div>
                            </div>
                            <div class="prog-total__pr">343 509 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="29"></div>
                                <div class="percircle__inn">2017</div>
                            </div>
                            <div class="prog-total__pr">324 259 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="29"></div>
                                <div class="percircle__inn">2017</div>
                            </div>
                            <div class="prog-total__pr">19 250 ₽</div>
                        </div>
                    </div>
                    <div class="prog-total__slide">
                        <div class="prog-total__row">
                            <div class="prog-total__date">2018</div>
                            <div class="prog-total__height">
                                <div class="prog-total__line" style="height: 85%"></div>
                            </div>
                            <div class="prog-total__pr">353 009 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="35"></div>
                                <div class="percircle__inn">2018</div>
                            </div>
                            <div class="prog-total__pr">331 959 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="35"></div>
                                <div class="percircle__inn">2018</div>
                            </div>
                            <div class="prog-total__pr">21 050 ₽</div>
                        </div>
                    </div>
                    <div class="prog-total__slide">
                        <div class="prog-total__row">
                            <div class="prog-total__date">2019</div>
                            <div class="prog-total__height">
                                <div class="prog-total__line" style="height: 75%"></div>
                            </div>
                            <div class="prog-total__pr">324 559 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="17"></div>
                                <div class="percircle__inn">2019</div>
                            </div>
                            <div class="prog-total__pr">306 209 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="17"></div>
                                <div class="percircle__inn">2019</div>
                            </div>
                            <div class="prog-total__pr">18 350 ₽</div>
                        </div>
                    </div>
                    <div class="prog-total__slide">
                        <div class="prog-total__row">
                            <div class="prog-total__date">2020</div>
                            <div class="prog-total__height">
                                <div class="prog-total__line" style="height: 80%"></div>
                            </div>
                            <div class="prog-total__pr">339 859 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="22"></div>
                                <div class="percircle__inn">2020</div>
                            </div>
                            <div class="prog-total__pr">321 509 ₽</div>
                        </div>
                        <div class="prog-total__row">
                            <div class="pie">
                                <div class="percircle" data-percircle="22"></div>
                                <div class="percircle__inn">2020</div>
                            </div>
                            <div class="prog-total__pr">18 350 ₽</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="prog-count">
        <div class="container">
            <div class="prog__subtitle">Итоги государственной программы</div>
            <div class="prog-count__flex">
                <div class="prog-count__red">
                    <div class="prog-count__icon">
                        <img src="/local/templates/rospatriot/static/img/general/iz1.png" alt="">
                    </div>
                    <div class="prog-count__text">Региональные программы и региональные центры патриотического воспитания во всех регионах России</div>
                </div>
                <div class="prog-count__red">
                    <div class="prog-count__icon">
                        <img src="/local/templates/rospatriot/static/img/general/iz2.png" alt="">
                    </div>
                    <div class="prog-count__text">Более 50 тысяч подготовленных организаторов и специалистов в сфере патриотического воспитания</div>
                </div>
            </div>
            <div class="prog-count__boxs">
                <div class="prog-count__box">
                    <div class="prog-count__title">В 2 РАЗА</div>
                    <div class="prog-count__name">увеличится количество выполнивших нормативы «ГТО» в общей численности участников всероссийского физкультурно-спортивного комплекса «ГТО»</div>
                </div>
                <div class="prog-count__box">
                    <div class="prog-count__title">В 2 РАЗА</div>
                    <div class="prog-count__name">увеличится количество студентов и школьников, знающих историю и культуру России, своего города и региона</div>
                </div>
                <div class="prog-count__box">
                    <div class="prog-count__title">В 1,5 РАЗА</div>
                    <div class="prog-count__name">увеличится количество вузов, на базе которых созданы и действуют волонтерские организации</div>
                </div>
                <div class="prog-count__box">
                    <div class="prog-count__title">В 3 РАЗА</div>
                    <div class="prog-count__name">увеличится количество образовательных организаций, трудовых коллективов, бизнес-структур, городов и районов, воинских частей (кораблей), участвующих в шефской работе</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::prog -->

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>