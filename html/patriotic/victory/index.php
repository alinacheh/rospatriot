<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Навстречу 75-летию Победы");
?>

<div class="document__content">
	<div class="header"></div>
	<div class="years">
		<div class="years-box">
			<div class="container">
				<div class="years__title page-title">Навстречу 75-летию Победы</div>
				<div class="years__text page-text">ФГБУ «Роспатриотцентр» – координатор деятельности рабочей группы по организации работы с волонтерами Организационного комитета по подготовке и проведению празднования 75-й годовщины Победы в Великой Отечественной войне 1941-1945
					годов.
				</div>
				<div class="doc__item news-item">
					<div class="doc-flex doc-sb">
						<div>
							<div class="doc__name">Приказ о формировании рабочей группы по организации работы с волонтерами Организационного комитета по подготовке и проведению празднования 75-й годовщины Победы в Великой Отечественной войне 1941–1945 годов</div>
							<div class="doc-flex">
								<div class="doc__type">Другие</div>
								<div class="doc__date">Добавлен: 07 Ноября 2019</div>
							</div>
						</div>
						<div class="doc__file">
							<a download="" href="/upload/iblock/5c9/318_от_13_09_2019_о_О_формировании.pdf" class="doc__download">
								<span class="icon">
									<svg class="icon__file-download-r" width="24px" height="24px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
									</svg>
								</span>
							</a>
							<div class="doc__size">PDF, 2.32 МБ</div>
						</div>
					</div>
				</div>
				<div class="years__subtitle">Функции рабочей группы</div>
				<div class="years-box__boxs">
					<div class="years-box__flex">
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">1</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/1.png" alt="">
									</div>
									<div class="years-box__text">Создание региональных рабочих групп по работе с волонтерами
									</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">2</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/2.png" alt="">
									</div>
									<div class="years-box__text">Разработка концепции волонтерского сопровождения мероприятий
									</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">3</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/3.png" alt="">
									</div>
									<div class="years-box__text">Разработка фирменного стиля волонтерского корпуса</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">4</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/4.png" alt="">
									</div>
									<div class="years-box__text">Определение основных мест формирования волонтерского корпуса
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="years-box__flex">
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">5</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/5.png" alt="">
									</div>
									<div class="years-box__text">Формирование единого событийного плана работы волонтерского корпуса, в том числе в регионах Российской Федерации</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">6</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/6.png" alt="">
									</div>
									<div class="years-box__text">Разработка образовательной программы для волонтеров по функциональным направлениям</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">7</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/7.png" alt="">
									</div>
									<div class="years-box__text">Набор волонтеров в основных местах формирования волонтерского корпуса и регионах</div>
								</div>
							</div>
						</div>
					</div>
					<div class="years-box__flex">
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">8</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/8.png" alt="">
									</div>
									<div class="years-box__text">Организация и проведение обучающих семинаров для волонтеров в регионах</div>
								</div>
							</div>
						</div>
						<div class="years-box__wrapper">
							<div class="years-box__box">
								<div class="years-box__number">9</div>
								<div class="years-box__white">
									<div class="years-box__icon">
										<img src="/local/templates/rospatriot/static/img/general/9.png" alt="">
									</div>
									<div class="years-box__text">Реализация плана основных федеральных мероприятий, посвященных 75-летию Победы в Великой Отечественной войне</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="years-project">
			<div class="container">
				<div class="years__subtitle">Проекты в рамках празднования 75-летия Победы</div>
				<div class="years__text page-text">В период с 2019 по 2020 год ФГБУ «Роспатриотцентр» запланирован целый комплекс мероприятий, приуроченных к празднованию 75-й годовщины Победы в Великой Отечественной войне 1941–1945 годов, а также посвященных проведению в Российской Федерации Года памяти и славы.</div>
				<div class="years-project__flex">
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/bessmertnyi-polk.jpg)">
						</div>
						<div class="years-project__content">
							<div class="years-project-front">Проведение торжественных мероприятий в субъектах Российской Федерации, волонтерское сопровождение парадов Победы и народного шествия «Бессмертный полк»</div>
							<div class="years-project-back">Волонтерское сопровождение торжественных мероприятий в субъектах Российской Федерации, в том числе, Парадов Победы и народного шествия «Бессмертный полк» (в т.ч. отбор добровольцев и проведение образовательной программы для волонтеров по Единому стандарту подготовки)</div>
						</div>
					</div>
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/vahta-pamyati.jpg)">
						</div>
						<div class="years-project__content">
							<a class="years-project__link" href="http://rf-poisk.ru/project/13/" target="_blank">
								<div class="years-project-front">Торжественное открытие международной акции «Вахта памяти – 2020»</div>
								<div class="years-project-back">Проведение всероссийской акции с целью почтить память погибших в Великой Отечественной войне, приуроченная к открытию поискового сезона по всей стране и в иностранных государствах</div>
							</a>
						</div>
					</div>
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/subbotnik.jpg)">
						</div>
						<div class="years-project__content">
							<div class="years-project-front">Международный субботник по благоустройству памятных мест и воинских захоронений</div>
							<div class="years-project-back">Организация субботника в формате «Дней единых действий» по всейстране и в странах-участницахМеждународной команды 75-летия Победы, в рамках которого волонтерызаймутся благоустройствомаллей славы, памятных мест и воинских захоронений</div>
						</div>
					</div>
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/sudba_soldata.jpg)">
						</div>
						<div class="years-project__content">
							<a class="years-project__link" href="http://rf-poisk.ru/project/2/" target="_blank">
								<div class="years-project-front">Передвижная выставка «Фронтовой портрет. Судьба солдата»</div>
								<div class="years-project-back">Организация передвижной экспозиции с изображениями солдат Красной армии из фронтовой графики. Проект призван установить судьбы солдат Красной армии, изображенных на фронтовых портретах, разыскать их родственников и вручить им собранную информацию и копии рисунков</div>
							</a>
						</div>
					</div>
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/ves-v-deda.jpg)">
						</div>
						<div class="years-project__content">
							<div class="years-project-front">Всероссийский проект «Весь в деда»</div>
							<div class="years-project-back">Проект, в рамках которого в современных форматах будут рассказаны истории героев, которые спасли нашу страну в войне, и истории их прямых потомков – молодых людей, живущих сейчас</div>
						</div>
					</div>
					<div class="years-project__box">
						<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/komanda-volonterov.jpg)">
						</div>
						<div class="years-project__content">
							<a class="years-project__link" href="волонтерыпобеды.рф" target="_blank">
								<div class="years-project-front">Проект «Международная команда волонтеров 75-летия Победы»</div>
								<div class="years-project-back">Формирование волонтерских команд в разных странах в целях помощи в организации ключевых событий празднования 75-летия Победы в Великой Отечественной войне 1941-1945 годов</div>
							</a>
						</div>
					</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/default-img.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Всероссийский патриотический форум</div>
								<div class="years-project-back">Организация итогового мероприятия года празднования 75-летия Победы в Великой Отечественной войне 1941–1945 годов с участием представителей государственных и некоммерческих организаций в сфере патриотического воспитания, поощрение волонтеров</div>
							</div>
						</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/posly_pobedy.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Международный конкурс «Послы Победы»</div>
								<div class="years-project-back">Отбор лучших добровольцев для помощи в организации Дня Победы в г. Москве и в другом Городе-Герое. В конкурсе могут принять участие активисты в возрасте от 18 лет, которые в течение всего года помогали по основным направлениям деятельности Всероссийского общественного движения «Волонтеры Победы». Победители Конкурса станут волонтерами парада Победы на Красной площади в г. Москве другом Городе-Герое</div>
							</div>
						</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/dni-edinyh-deistviy.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Акции в формате «Дни единых действий»</div>
								<div class="years-project-back">
									<p>Всероссийская акция<br>«Георгиевская ленточка»</p>
									<p>Международная акция<br>«Улицы Героев»</p>
									<p>Международная акция<br>«Письмо Победы»</p>
								</div>
							</div>
						</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/pobeda-odna-na-vseh.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Всероссийский сбор военно-патриотических объединений «Победа одна на всех» с участием делегаций государств - участников Содружества Независимых Государств</div>
								<div class="years-project-back">Организация международной профильной смены во Всероссийском детском центре «Орленок» для детей возрастом до 18 лет, проживающих в странах СНГ, с целью сохранения исторической памяти о значимости общего вклада иностранных государств в Победу в Великой Отечественной войне 1941–1945 годов. Проведение финала всероссийской военно-патриотической игры «Орленок»</div>
							</div>
						</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/default-img.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Всероссийский конкурс граффити в малых городах России, посвященного изображению маршалов Победы и героев Великой Отечественной войны 1941-1945 годов</div>
								<div class="years-project-back">Организация отбора лучших стрит-арт работ художников из малых городов страны с целью популяризации подвигов Героев Великой Отечественной войны 1941–1945 годов, формирующих позитивный образ нашей страны, укрепления чувств сопричастности граждан к великой истории и культуре России, а также благоустройства городской среды</div>
							</div>
						</div>
						<div class="years-project__box years-project__more">
							<div class="years-project__image" style="background-image:url(/local/templates/rospatriot/static/img/general/projects/default-img.jpg)">
							</div>
							<div class="years-project__content">
								<div class="years-project-front">Всероссийский проект «Забег Победы»</div>
								<div class="years-project-back">Организация комплекса спортивных мероприятий с использование современных форматов с целью создания условий для формирования чувства сопричастности у граждан с историческими событиями Великой Отечественной войны 1941–1945 годов и личной ответственности за будущее родной страны</div>
							</div>
						</div>
				</div>
					<div class="years-project__footer">
						<a href="#" class="btn btn--base years-project__toggle">Загрузить еще</a>
					</div>
			</div>
		</div>
		<div class="years-double years-gray">
			<div class="container">
				<div class="years__subtitle">Фасилитационная сессия в формате «Open Space» на федеральных
					<br>молодежных форумах
				</div>
				<div class="years-double__row row">
					<div class="col-lg-6">
						<div class="html">
							<p>«Роспатриотцентр» в рамках федеральных молодежных форумов организует сессии по разработке идей для социальных проектов и обсуждения спорных вопросов.</p>
							<div class="years-double__red">Технология открытого пространства (ТОП) — методика проведения семинаров и конференций с единой глобальной тематикой, но без формальной повестки, программы, кроме известной общей цели. ТОП представляет собой самоорганизующийся
								процесс.
							</div>
							<p>Самые актуальные и интересные идеи и проекты попадут в федеральный план мероприятий, посвященных празднованию 75-й годовщины Победы в Великой Отечественной войне.</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="years-double__wrapper">
							<div class="years-double__image" style="background-image: url(/local/templates/rospatriot/static/img/general/years-image1.jpg)">
								<img src="/local/templates/rospatriot/static/img/general/years-image1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="years-double">
			<div class="container">
				<div class="years__subtitle">Международный волонтерский корпус 75-летия Победы</div>
				<div class="years-double__row row">
					<div class="col-lg-6">
						<div class="html">
							<p>Помогать в проведении мероприятий, посвященных 75-й годовщине Победы в ВОВ, будут члены международного волонтерского корпуса.</p>
							<div class="years-double__center">
								<img src="/local/templates/rospatriot/static/img/general/vol.png" alt="">
							</div>
							<a href="https://волонтёрыпобеды.рф" target="_blank" class="btn years-double__btn">волонтёрыпобеды.рф</a>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="years-double__wrapper">
							<div class="years-double__image" style="background-image: url(/local/templates/rospatriot/static/img/general/years-image2.jpg)">
								<img src="/local/templates/rospatriot/static/img/general/years-image2.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>