<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "РОСПАТРИОТЦЕНТР - Грантовый конкурс");
$APPLICATION->SetPageProperty("keywords",
    "роспатриотцентр, патриотизм, воспитание молодежи, патриотика, 75 лет победы");
$APPLICATION->SetPageProperty("description",
    "Конкурс на предоставление грантов в форме субсидий из федерального бюджета некоммерческим организациям, в том числе молодежным и детским общественным объединениям (за исключением казенных учреждений), на проведение мероприятий по содействию патриотическому воспитанию граждан Российской Федерации.");
$APPLICATION->SetTitle("Грантовый конкурс");
?>
<div class="grant">
    <div class="container">
        <div class="page-title">
            Грантовый конкурс
        </div>
        <div class="page-text">
            Конкурс на предоставление грантов в форме субсидий из федерального бюджета некоммерческим организациям,
            в том числе молодежным и детским общественным объединениям (за исключением казенных учреждений), на
            проведение мероприятий по содействию патриотическому воспитанию граждан Российской Федерации.
        </div>
        <div class="grant__desc">
            Конкурс проходит в рамках реализации государственной программы «Патриотическое воспитание граждан
            Российской Федерации на 2016 – 2020 годы»
        </div>
        <div class="grant-napr">
            <div class="grant-title">
                Грантовые направления
            </div>
            <div class="grant-napr__flex">
                <div class="grant-napr__box">
                    <div class="grant-napr__image">
                        <img src="/local/templates/rospatriot/static/img/general/grant1.jpg" alt="">
                    </div>
                    <div class="grant-napr__text">
                        Подготовка и проведение военно-исторических реконструкций
                    </div>
                </div>
                <div class="grant-napr__box">
                    <div class="grant-napr__image">
                        <img src="/local/templates/rospatriot/static/img/general/grant2.jpg" alt="">
                    </div>
                    <div class="grant-napr__text">
                        Поддержка молодежных поисковых отрядов и объединений
                    </div>
                </div>
                <div class="grant-napr__box">
                    <div class="grant-napr__image">
                        <img src="/local/templates/rospatriot/static/img/general/grant3.jpg" alt="">
                    </div>
                    <div class="grant-napr__text">
                        Реализация проектов патриотической направленности, реализуемых волонтерскими
                        (добровольческими) организациями
                    </div>
                </div>
            </div>
        </div>
        <div class="grant-date">
            <div class="grant-date__box">
                <div class="grant-date__year">
                    2016
                </div>
                <div class="grant-date__title">
                    заявок
                </div>
                <div class="grant-date__number">
                    143
                </div>
                <div class="grant-date__title">
                    победителей
                </div>
                <div class="grant-date__number">
                    40
                </div>
            </div>
            <div class="grant-date__box">
                <div class="grant-date__year">
                    2017
                </div>
                <div class="grant-date__title">
                    заявок
                </div>
                <div class="grant-date__number">
                    315
                </div>
                <div class="grant-date__title">
                    победителей
                </div>
                <div class="grant-date__number">
                    47
                </div>
            </div>
            <div class="grant-date__box">
                <div class="grant-date__year">
                    2018
                </div>
                <div class="grant-date__title">
                    заявок
                </div>
                <div class="grant-date__number">
                    286
                </div>
                <div class="grant-date__title">
                    победителей
                </div>
                <div class="grant-date__number">
                    47
                </div>
            </div>
            <div class="grant-date__box">
                <div class="grant-date__year">
                    2019
                </div>
                <div class="grant-date__title">
                    заявок
                </div>
                <div class="grant-date__number">
                    418
                </div>
                <div class="grant-date__title">
                    победителей
                </div>
                <div class="grant-date__number">
                    42
                </div>
            </div>
        </div>
        <div class="grant-expande">
            <div class="grant-expande__box active">
                <div class="grant-expande__header">
                    <div class="grant-expande__title">
                        Какие некоммерческие организации могут быть участниками конкурса?
                    </div>
                    <div class="grant-expande__more">
                    </div>
                </div>
                <div class="grant-expande__content">
                    <div class="html">
                        <ul>
                            <li>не находящиеся в процессе ликвидации, банкротства, под действием решения суда о
                                приостановлении деятельности;
                            </li>
                            <li>не являющиеся иностранным юридическим лицом, а также российским юридическим лицом, в
                                уставном (складочном) капитале которого доля участия иностранных юридических лиц,
                                местом регистрации которых является государство или территория, включенные в
                                утвержденный Министерством финансов Российской Федерации перечень государств и
                                территорий, предоставляющих льготный налоговый режим налогообложения и (или) не
                                предусматривающих раскрытия и предоставления информации при проведении финансовых
                                операций (офшорные зоны), в совокупности превышает 50 процентов;
                            </li>
                            <li>не получают средства из федерального бюджета в соответствии с иными правовыми актами
                                на цели, указанные в пункте 2 Правил предоставления грантов в форме субсидий из
                                федерального бюджета некоммерческим организациям, в том числе молодежным и детским
                                общественным объединениям (за исключением казенных учреждений), на проведение
                                мероприятий по содействию патриотическому воспитанию граждан Российской Федерации,
                                утвержденных постановлением Правительства Российской Федерации от 13 августа 2016 г.
                                № 795;
                            </li>
                            <li>у организации отсутствует просроченная задолженность по возврату;</li>
                            <li>в федеральный бюджет субсидий, бюджетных инвестиций, предоставленных;</li>
                            <li>в том числе в соответствии с иными правовыми актами, и иная просроченная
                                задолженность перед федеральным бюджетом.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grant-expande__box">
                <div class="grant-expande__header">
                    <div class="grant-expande__title">
                        На основании каких критериев проводится оценка проектов, представленных в составе заявок?
                    </div>
                    <div class="grant-expande__more">
                    </div>
                </div>
                <div class="grant-expande__content">
                    <div class="html">
                        <p>
                            Оценка проектов, представленных в составе заявок, проводится на основании следующих
                            критериев:
                        </p>
                        <ul>
                            <li>актуальность и социальная значимость проекта;</li>
                            <li>логическая связанность мероприятий проекта, их соответствие целям, задачам и
                                ожидаемым результатам;
                            </li>
                            <li>инновационность и уникальность проекта;</li>
                            <li>соотношение планируемых расходов на реализацию проекта и его ожидаемых результатов,
                                адекватность, измеримость и достижимость таких результатов;
                            </li>
                            <li>реалистичность и обоснованность планируемых расходов на реализацию проекта;</li>
                            <li>масштаб реализации проекта;</li>
                            <li>собственный вклад и дополнительные ресурсы, привлекаемые для реализации проекта,
                                перспективы его дальнейшего развития;
                            </li>
                            <li>опыт успешной реализации проектов организации;</li>
                            <li>наличие опыта и соответствующих компетенций сотрудников организации или привлекаемых
                                специалистов для реализации проекта;
                            </li>
                            <li>информационная открытость, публичность организации.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="grant-stage">
            <div class="grant-title">
                Этапы конкурса
            </div>
            <div class="social-comp__flex">
                <div class="social-comp__boz">
                    <div class="social-comp__num">
                        1
                    </div>
                    <div class="social-comp__sec">
                        Прием заявок
                    </div>
                    <div class="social-comp__desc">
                        Заявки заполняются <br>
                        и направляются оператору <br>
                        Конкурса
                    </div>
                </div>
                <div class="social-comp__boz">
                    <div class="social-comp__num">
                        2
                    </div>
                    <div class="social-comp__sec">
                        Экспертная оценка
                    </div>
                    <div class="social-comp__desc">
                        Допущенные до участия <br>
                        в конкурсе заявки <br>
                        распределяются экспертам <br>
                        (каждая – не менее чем <br>
                        3 экспертам)
                    </div>
                </div>
                <div class="social-comp__boz">
                    <div class="social-comp__num">
                        3
                    </div>
                    <div class="social-comp__sec">
                        Определение игрового рейтинга проектов
                    </div>
                    <div class="social-comp__desc">
                        На основании результатов <br>
                        оценки проектов конкурсная <br>
                        комиссия присваивает каждой <br>
                        заявке порядковый номер <br>
                        в порядке уменьшения <br>
                        присвоенного балла заявке
                    </div>
                </div>
                <div class="social-comp__boz">
                    <div class="social-comp__num">
                        4
                    </div>
                    <div class="social-comp__sec">
                        Итоги конкурса
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grant-banner">
        <div class="container">
            <div class="grant-banner__banner">
                <div class="grant-banner__title">
                    Победители конкурса в 2019 году
                </div>
                <a href="winner.php" class="grant-banner__btn btn btn-base">Смотреть всех</a>
            </div>
        </div>
    </div>
    <div class="grant-doc">
        <div class="container">
            <div class="grant-title">
                Документы
            </div>
            <div class="grant-doc__flex">
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">Требования, предъявляемые к отчетности Получателя гранта в форме субсидии из федерального бюджета</div>
                            <div class="doc-flex">
                                <div class="doc__type">Протоколы</div>
                                <div class="doc__date">Добавлен: 14 февраля 2019</div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download href="/upload/competition//doc17.pdf" class="doc__download">
                                            <span class="icon">
                <svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg>
            </span>
                            </a>
                            <div class="doc__size">PDF, 218 КБ</div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">Форма отчета о реализации проекта</div>
                            <div class="doc-flex">
                                <div class="doc__type">Протоколы</div>
                                <div class="doc__date">Добавлен: 14 февраля 2019</div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download href="/upload/competition//doc18.docx" class="doc__download">
                                            <span class="icon">
                    <svg class="icon__file-download-r" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                    </svg>
                </span>
                            </a>
                            <div class="doc__size">DOCX, 62 КБ</div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                Конкурсная документация
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc1.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg>
                                </span>
                            </a>
                            <div class="doc__size">
                                PDF, 279 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                Методические рекомендации по подготовке
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc2.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 4 211 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                ПРОТОКОЛ 1 от 25.03.2019 вскрытие
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc3.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 4 353 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                ПРОТОКОЛ 2 от 27.03.2019 список
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc4.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 2 182 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                ПРОТОКОЛ 3 от 17.04.2019 список
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc5.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 4 008 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                СТАТИСТИКА ЗАЯВОК
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc6.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 685 КБ
                            </div>
                        </div>
                    </div>
                </div>
                <div class="doc__item">
                    <div class="doc-flex doc-sb">
                        <div>
                            <div class="doc__name">
                                СТАТИСТИКА ПОБЕДИТЕЛИ
                            </div>
                            <div class="doc-flex">
                                <div class="doc__type">
                                    Протоколы
                                </div>
                                <div class="doc__date">
                                    Добавлен: 14 февраля 2019
                                </div>
                            </div>
                        </div>
                        <div class="doc__file">
                            <a download="" href="/upload/competition/doc7.pdf" class="doc__download"> <span
                                        class="icon"><svg class="icon__file-download-r" width="24px" height="24px">
                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
                </svg> </span>
                            </a>
                            <div class="doc__size">
                                PDF, 576 КБ
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
