<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "Победители 2019 года");
$APPLICATION->SetPageProperty("title", "Победители 2019 года");
$APPLICATION->SetTitle("Победители 2019 года");
?>
<div class="winner">
                    <div class="container">
                        <div class="page-title">Победители 2019 года</div>
                        <div class="winner__content">
                            <div class="winner__menu">
                                <div class="winner__menu-item active">Подготовка и проведение военно-исторических реконструкций</div>
                                <div class="winner__menu-item">Поддержка молодежных поисковых отрядов и объединений</div>
                                <div class="winner__menu-item">Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</div>
                            </div>

                            <div class="winner__list">
                                <div class="winner__tabs">
                                    <div class="winner__tab" style="">
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Организация и проведение культурно-исторического фестиваля «Зарайский ратный сбор»</div>
                                                <div class="winner__item-more  active ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  show  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Центр интеллектуальной собственности «Группа «ВИЮР»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки; молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Московская область, г.о. Зарайск</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>750 000 руб</p>
                                                </div>
                                                <a href="http://www.viyur.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Всероссийский конкурс интерактивных военно-исторических реконструкций</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Агентство инновационного развития»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки; молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 400 000 руб</p>
                                                </div>
                                                <a href="http://www.rusinnovations.com" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/21.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-исторический проект «Исторический десант»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Военно-патриотический отряд «Легиона»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки; молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Татарстан, г. Казань</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 000 000 руб.</p>
                                                </div>
                                                <a href="http://пропоиск.рф" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/22.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-историческая реконструкция «Освобождение Жиздры. 1943 год»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Клуб военно-исторической реконструкции «Калужский Гарнизон»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций;</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>Жители Калужской области, военно-исторические клубы</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Калужская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>450 000 руб</p>
                                                </div>
                                                <a href="http://vk.com/kalugskyigarnizon" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/23.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">Региональный военно-исторический фестиваль «Служилые люди Сибири – Тарского острога», посвященный 425-летию г. Тара</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация духовно-патриотический спортивно-оздоровительный Центр «Застава Ермака»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Омская область, г. Омск</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 050 000 руб</p>
                                                </div>
                                                <a href="http://zastava-ermaka.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-историческая реконструкция «Они сражались за Родину»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Башкортостанское региональное отделение Всероссийской общественной организации «Молодая Гвардия Единой России»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь, военно-исторические реконструкторы, ветераны Великой Отечественной войны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Башкортостан, Туймазинский район</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>800 000 руб</p>
                                                </div>
                                                <a href="http://molgvardiarb.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/26.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">XI фестиваль исторической реконструкции «Царицын», приуроченный к празднованию 430-летию со дня основания Волгограда</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Волгоградская областная общественная организация «Военно-исторический клуб «Шатун»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>участники общероссийского общественного движения «Клубы исторической реконструкции России», жители Волгограда</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Волгоградская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 700 000 руб.</p>
                                                </div>
                                                <a href="http://vk.com/shatun34" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/27.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-исторический фестиваль «Гроза над Ситой»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Дальневосточная межрегиональная военно-историческая общественная организация «Уссурийский фронт»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>пос. Сита района им. Лазо Хабаровского края</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://vk.com/ussurfront27" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/28.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">«Афганистан – живая память»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Забайкальская региональная общественная организация «Клуб военно-исторической реконструкции «Забайкальский фронт»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>школьники и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Забайкальский край</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>500 000 руб.</p>
                                                </div>
                                                <a href="http://vk.com/vovrekon75" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Сибирь изначальная»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Кемеровская городская молодежная общественная организация «Военно-исторический клуб «Единорог»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки; молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Новосибирская область. г. Новосибирск</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://вик-единорог.рф" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Цикл фестивалей «Живая История»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Липецкая областная молодёжная общественная организация «Военно-исторический клуб «Копьё»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Липецкая область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>950 000 руб</p>
                                                </div>
                                                <a href="http://www.south-rus.org/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-историческая реконструкция «На гребне «Карельского Вала»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Межрегиональная Общественная Организация содействия изучению отечественной военной истории «Эпоха»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>школьники и студенты, ветераны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Санкт-Петербург, Ленинградская область, Выборгский район, Рощинское сельское поселение</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>550 000 руб</p>
                                                </div>
                                                <a href="http://www.epoha-union.org/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Партизанская деревня»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общественная организация «Челябинский региональный экологический центр «Наш дом»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Челябинская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>500 000 руб</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/33.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Живая история»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Псковская региональная общественная организация «Военно-патриотический спортивный клуб «Шторм»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты, ветераны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Псковская область (Себежский район)</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 500 000 руб</p>
                                                </div>
                                                <a href="http://vympel-storm.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Плацдарм Невский пятачок»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Частное некоммерческое учреждение «музей узкоколейной железной дороги»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Ленинградская область, г. Санкт-Петербург</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>800 000 руб</p>
                                                </div>
                                                <a href="http://www.museum-uzd.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="winner__tab" style="display: none;">
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/35.gif" alt="">
                                                </div>
                                                <div class="winner__item-name">СЛЁТ ПОИСКОВЫХ ОТРЯДОВ. ON-LINE-формат</div>
                                                <div class="winner__item-more  active ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  show  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Государственное бюджетное учреждение Ямало-Ненецкого автономного округа «Региональный центр патриотического воспитания»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>поисковые отряды</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>11 муниципальных образований Ямало-Ненецкого автономного округа</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 620 000 руб.</p>
                                                </div>
                                                <a href="http://yamalrcpv.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/36.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">«Нам доверена Память»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Калмыцкая региональная общественная организация «Ассоциация поисковых отрядов «Калмыкия»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>школьники и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Калмыкия, Ростовская область, Астраханская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>295 000 руб</p>
                                                </div>
                                                <a href="http://kalmpoisk.r08.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Герои карельского фронта»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Карельская республиканская общественная организация по поиску и увековечению памяти погибших при защите Отечества «Союз поисковых отрядов Карелии»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Карелия</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>647 000 руб</p>
                                                </div>
                                                <a href="http://karelfront.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/38.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Нам доверена Память…»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общероссийское общественное движение по увековечению памяти погибших при защите Отечества «Поисковое движение России»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>школьники и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>3 473 500 руб.</p>
                                                </div>
                                                <a href="http://рф-поиск.рф/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/39.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">«Имена из солдатских медальонов – авиаторы»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Региональная общественная молодежная организация Республики Татарстан «Объединение «Отечество»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>поисковых отряды ООД «Поисковое движение России»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Татарстан, Республика Карелия, г.Москва, Ленинградская обл., г.Санкт-Петербург и другие регионы, где работают поисковые отряды ООД «Поисковое движение России», специализирующиеся на авиационном
                                                        поиске</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>500 000 руб</p>
                                                </div>
                                                <a href="http://отечестворт.рф" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«От истоков Суры до могилы героев»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Региональное отделение общероссийского движения по увековечиванию памяти погибших при защите отечества «Поисковое движение России» в Пензенской области</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты, ветераны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Республика Карелия, Пензенская, Орловская, Новгородская и Тверская области, Республика Беларусь</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>545 000 руб</p>
                                                </div>
                                                <a href="http://poisk-penza.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/41.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">«Прикоснись к истории»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Хабаровская краевая общественная организация по содействию в патриотическом воспитании граждан «Амурский рубеж»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Подготовка и проведение военно-исторических реконструкций</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Хабаровский край</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://vk.com/amur_r" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Достойная смена»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Челябинская региональная общественная организация «ресурсный центр поисковой деятельности и специальной подготовки «Опора»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Поддержка молодежных поисковых отрядов и объединений</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты, дети из числа семей социального риска</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Челябинская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>919 000 руб</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="winner__tab" style="display: none;">
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Интерактивная школа волонтеров краеведческой работы Сибирского Федерального округа»</div>
                                                <div class="winner__item-more  active ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  show  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Агентство стратегических инноваций»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Краснодарский край, Новосибирская область, Омская область Томская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 000 000 руб</p>
                                                </div>
                                                <a href="http://anoasi.com" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/2.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Вектор мужества, от легендарного прошлого до героического настоящего»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Военно-патриотический центр «Вымпел»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 400 000 руб</p>
                                                </div>
                                                <a href="http://www.vpc-vympel.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Всероссийский проект «Аллея Героев»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Центр патриотического развития «Русич»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>муниципальные образования, учебные заведения, местные жители.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Владимирская область, Воронежская область, Калужская область, Липецкая область, Москва, Московская область, Рязанская область, Тамбовская область, Тульская область, ХМАО</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 500 000 руб</p>
                                                </div>
                                                <a href="http://patriot.center/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/4.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">«Эстафета поколений: атмосфера состязания».</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация «Центр содействия патриотическому воспитанию молодежи»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты, ветераны различных категорий разного возраста.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>700 000 руб.</p>
                                                </div>
                                                <a href="http://vk.com/club132659241" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/5.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Организация и проведение Всероссийского (IV этапа) финала детско-юношеской военно-спортивной игры «Зарница»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Автономная некоммерческая организация дополнительного образования «Центр патриотического воспитания, физической культуры и военно-прикладных видов спорта «Команда»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://komandacenter.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/6.webp" alt="">
                                                </div>
                                                <div class="winner__item-name">Серия патриотических акций «Серебряная нить. Связь поколений»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>АНО Общественно-гуманитарный центр «Молодежное сообщество»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты, ветераны Великой Отечественной войны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>г. Москва</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 700 000 руб</p>
                                                </div>
                                                <a href="http://mol-soobschestvo.com" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Патрида»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Ассоциация «Институт социальных перспектив»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 200 000 руб</p>
                                                </div>
                                                <a href="http://sk-news.ru/blogs/isp" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/8.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«МЫ С РОССИЕЙ ВМЕСТЕ – ДУША НА МЕСТЕ!»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Благотворительный фонд «Наследие Отечества»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и школьники, молодежь и студенты.</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Городское поселение Кратово, Раменского района Московской области, а также в г.о. Жуковский и в г.о. Бронницы</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 300 000 руб</p>
                                                </div>
                                                <a href="http://vkovchege.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/9.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">Организация и проведение соревнований по военно-тактическим играм (лазертаг) «Квазар» среди молодежи Ростовской области</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Благотворительный фонд «Солидарность. Добро. Милосердие.»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Ростовская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>500 000 руб</p>
                                                </div>
                                                <a href="http://sdm-fond.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Федеральная команда тренеров «Мастера»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Всероссийское общественное движение «Волонтеры Победы»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>600 000 руб</p>
                                                </div>
                                                <a href="http://волонтерыпобеды.рф" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Молодежная археологическая экспедиция «Забытая история»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Липецкая региональная научная общественная организация «Археологические исследования»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Добровский район Липецкой области</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>500 000 руб</p>
                                                </div>
                                                <a href="http://archlip48.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/12.jpg" alt="">
                                                </div>
                                                <div class="winner__item-name">Военно-патриотический форум «Альфовец»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Международная Ассоциация ветеранов подразделения антитеррора «Альфа»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>г. Москва, Воронежская, Рязанская, Липецкая, Свердловская, Челябинская, Омская, Владимирская, Саратовская, Калужская, Московская области, Пермский и Краснодарский края, Республики Башкортостан и Северная
                                                        Осетия</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://alphagroup.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/13.svg" alt="">
                                                </div>
                                                <div class="winner__item-name">Всероссийская интеллектуально-киберспортивная лига 2019</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общероссийская общественная организация «Федерация компьютерного спорта России»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и школьники</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 400 000 руб</p>
                                                </div>
                                                <a href="http://resf.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Всероссийский проект «Патриотика: контент +»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общероссийская общественная организация «Национальная родительская ассоциация социальной поддержки семьи и защиты семейных ценностей»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и родители</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Российская Федерация</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 450 000 руб</p>
                                                </div>
                                                <a href="http://www.nra-russia.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Союз братских народов»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общероссийское общественное движение содействия укреплению дружбы и согласия среди молодежи «Всероссийский межнациональный союз молодежи»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>г. Москва</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 300 000 руб</p>
                                                </div>
                                                <a href="http://vmsm.info" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">Международный волонтерский лагерь «Живу и помню!» (посвящается 75-летию освобождения Литвы от немецко-фашистских захватчиков)</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Общественное молодежное движение Псковской области «Лига молодежи Псковской области»</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты, ветераны Великой Отечественной войны</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Псковская область, Литва, Латвия</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>900 000 руб</p>
                                                </div>
                                                <a href="http://ligapskov.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/16.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Будь готов!»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Региональное отделение Всероссийского детско-юношеского военно-патриотического общественного движения «Юнармия» Оренбургской области</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>Оренбургская область</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>700 000 руб</p>
                                                </div>
                                                <a href="http://unarmy.cpv56.ru/" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/winner.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Как стать героем? Советы Героев!»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Региональный общественный фонд поддержки Героев Советского союза и Героев РФ имени генерала Е.Н. Кочешкова</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>молодежь и студенты</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>г. Москва</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 300 000 руб</p>
                                                </div>
                                                <a href="http://www.fondgeroev.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                        <div class="winner__item">
                                            <div class="winner__item-header">
                                                <div class="winner__item-logo">
                                                    <img src="/local/templates/rospatriot/static/img/general/winner/18.png" alt="">
                                                </div>
                                                <div class="winner__item-name">«Вальс Победы»</div>
                                                <div class="winner__item-more ">
                                                    <span>
                    <svg class="icon__down-arrow" width="129px" height="129px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#down-arrow"></use>
                    </svg>
                </span>
                                                </div>
                                            </div>
                                            <div class="winner__item-content  ">
                                                <div class="winner__item-text"></div>

                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__building-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Организация:</p>
                                                    <p>Фонд стратегических инициатив музея Победы</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__bill-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#bill-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Грантовое направление:</p>
                                                    <p>Реализация проектов патриотической направленности, реализуемых волонтерскими (добровольческими) организациями</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__group-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#group-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">Целевая группа:</p>
                                                    <p>дети и подростки, молодежь и студенты, пенсионеры</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__earth-line" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#earth-line"></use>
                    </svg>
                </span>
                                                    <p class="gray">География проекта:</p>
                                                    <p>г. Москва</p>
                                                </div>
                                                <div class="winner__item-info">
                                                    <span class="icon">
                    <svg class="icon__sum" width="32px" height="32px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#sum"></use>
                    </svg>
                </span>
                                                    <p class="gray">Сумма гранта:</p>
                                                    <p>1 600 000 руб</p>
                                                </div>
                                                <a href="http://victorymuseum.ru" class="btn winner__item-link">Перейти на страницу проекта</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>