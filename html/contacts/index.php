<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "роспатриотцентр, волонтерство, патриотизм, воспитание молодежи, патриотика, волонтер, добровольчество, 75 лет победы");
$APPLICATION->SetPageProperty("description", "Контакты ФГБУ «Роспатриотцентр». 121099, Москва, Новинский бульвар д. 3 стр. 1,  +7 (499) 967-86-70 , rospatriotcentr@rospatriotcentr.ru");
$APPLICATION->SetPageProperty("title", "РОСПАТРИОТЦЕНТР - Контакты");
$APPLICATION->SetTitle("Контактная информация");
?><div class="document__content">
	<div class="header">
	</div>
	<div class="contact">
		<div class="container">
			<div class="title-page">
				 Контакты ФГБУ «Роспатриотцентр»
			</div>
		</div>
		<div class="container min-bg-white">
			<div class="contact__address">
				<div class="contact__address-title">
					 Федеральное государственное бюджетное учреждение «Российский центр гражданского и патриотического воспитания детей и молодежи»
				</div>
				<div class="contact__address-list">
					<div class="contact__address-item first">
 <span class="icon"><svg class="icon__building-fill" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#building-fill"></use>
                    </svg></span>
						121099, Москва, Новинский бульвар д. 3 стр. 1
					</div>
					<div class="contact__address-item third">
 <span class="icon"><svg class="icon__metro-fill" width="24px" height="22px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#metro-fill"></use>
                    </svg></span>
						<ul class="metro-list">
							<li class="metro-item">Смоленская</li>
							<li class="metro-item">Смоленская</li>
							 <!-- <li class="metro-item">Баррикадная</li> -->
						</ul>
					</div>
					<div class="contact__address-item second">
 <span class="icon"><svg class="icon__phone-fill" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#phone-fill"></use>
                    </svg></span> <a href="tel:+7 (499) 967-86-70">+7 (499) 967-86-70</a>
					</div>
					<div class="contact__address-item fourth">
 <span class="icon"><svg class="icon__mail-fill" width="24px" height="24px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#mail-fill"></use>
                    </svg></span> <a href="mailto:rospatriotcentr@rospatriotcentr.ru">rospatriotcentr@rospatriotcentr.ru</a>
					</div>
				</div>
			</div>
		</div>
		<div class="contact__map" id="contact__map">
		</div>
		<div class="min-bg-white">
			<div class="container">
				 <?$APPLICATION->IncludeComponent(
	"altasib:feedback.form",
	"contacts",
	Array(
		"ACTIVE_ELEMENT" => "Y",
		"ADD_HREF_LINK" => "Y",
		"ADD_LEAD" => "N",
		"ALX_LINK_POPUP" => "N",
		"BBC_MAIL" => "",
		"CAPTCHA_TYPE" => "recaptcha",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"CHANGE_CAPTCHA" => "N",
		"CHECKBOX_TYPE" => "CHECKBOX",
		"CHECK_ERROR" => "Y",
		"COLOR_OTHER" => "#009688",
		"COLOR_SCHEME" => "BRIGHT",
		"COLOR_THEME" => "",
		"COMPONENT_TEMPLATE" => "contacts",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"FB_TEXT_NAME" => "Текст сообщения (не более 5 000 символов)",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FORM_ID" => "1",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "altasib_feedback",
		"INPUT_APPEARENCE" => array(0=>"DEFAULT",),
		"JQUERY_EN" => "N",
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
		"LOCAL_REDIRECT_ENABLE" => "N",
		"MASKED_INPUT_PHONE" => array(0=>"PHONE",),
		"MESSAGE_OK" => "Ваше сообщение было успешно отправлено",
		"NAME_ELEMENT" => "FIO",
		"NOT_CAPTCHA_AUTH" => "Y",
		"PROPERTY_FIELDS" => array(0=>"PHONE",1=>"FIO",2=>"EMAIL",3=>"TEXT_MESSAGE",4=>"FILE",),
		"PROPERTY_FIELDS_REQUIRED" => array(0=>"FIO",1=>"EMAIL",2=>"TEXT_MESSAGE",),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(0=>"EMAIL",),
		"PROPS_AUTOCOMPLETE_NAME" => array(0=>"FIO",),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(0=>"PHONE",),
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"RECAPTCHA_THEME" => "light",
		"RECAPTCHA_TYPE" => "image",
		"REQUIRED_SECTION" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"SECTION_MAIL_ALL" => "rospatriotcentr@rospatriotcentr.ru",
		"SEND_IMMEDIATE" => "Y",
		"SEND_MAIL" => "N",
		"SHOW_LINK_TO_SEND_MORE" => "Y",
		"SHOW_MESSAGE_LINK" => "Y",
		"SPEC_CHAR" => "N",
		"USERMAIL_FROM" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_INPUT_LABEL" => "Я ознакомлен(-на) с <a href=\"javascript:void(0)\">Правилами обработки 										персональных данных</a> и даю своё согласие на их обработку.",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_CAPTCHA" => "Y",
		"WIDTH_FORM" => ""
	)
);?>
			</div>
		</div>
	</div>
	 <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
