<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 16.10.19
 * Time: 13:08
 */

require('httpClient.php');
$_SERVER["DOCUMENT_ROOT"] = dirname(__DIR__);

require_once(dirname(__DIR__) . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

define('AUTH_BASIC', 'ros:patriot');
define('NEWS_PER_REQUEST', 1);
define('IBLOCK_ID', 1);
define('IBLOCK_SECTION_ID', false);
define('FORCE_UPDATE', false);
define('NEWS_PER_RUN', 5);

libxml_use_internal_errors(true);
$baseUrl = 'http://old.rospatriotcentr.ru';
$uploadDir = dirname(__DIR__) . '/upload/news/';
$tmpDir = dirname(__DIR__) . '/upload/tmp/news/';

if (!is_dir($uploadDir)) {
    mkdir($uploadDir, 0755, true);
}
if (!is_dir($tmpDir)) {
    mkdir($tmpDir, 0755, true);
}

$newsRes = CIBlockElement::GetList(
    ["SORT" => "ASC"],
    [
        'XML_ID' => 'import_%',
    ]
);
$el = new CIBlockElement();

while ($arNews = $newsRes->Fetch()) {
    $text = preg_replace('#<b>([^<]+)<\/b>#i', '', $arNews['DETAIL_TEXT'], 1);
    $arUpdateFields = [
        'DETAIL_TEXT' => $text,
    ];
    print 'Fix news ' . $arNews['ID'] . PHP_EOL;
    $extId = (int)preg_replace('#import_#', '', $arNews['XML_ID']);
    if (!$arNews['PREVIEW_PICTURE'] && $extId) {
        $url = $baseUrl . '/region_news/' . $extId;
        $news = httpClient::get($url);
        $news = trim($news);
        $news = mb_convert_encoding($news, 'HTML-ENTITIES', "UTF-8");
        $doc = new DOMDocument();
        $doc->encoding = 'UTF-8';
        $doc->loadHTML($news);
        $xpath = new DOMXpath($doc);

        $mainImage = $xpath->query('//img[@class="mainImgOfArt"]');
        if ($mainImage->length > 0) {
            $src = $mainImage->item(0)->getAttribute('src');
            print 'Main image: ' . $src . PHP_EOL;
            $name = $tmpDir . md5($src) . '.jpg';
            if (!file_exists($name)) {
                $image = httpClient::get($baseUrl . $src);
                $fh = fopen($name, 'wb');
                fwrite($fh, $image);
                fclose($fh);
                unset($image, $fh);
            }
            $arUpdateFields['PREVIEW_PICTURE'] = CFile::MakeFileArray($name);
            unset($src, $name);
        }
        unset($mainImage);
    }

    $el->Update($arNews['ID'], $arUpdateFields);
}
