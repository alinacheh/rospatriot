<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 18.09.19
 * Time: 11:27
 */

require('httpClient.php');
$_SERVER["DOCUMENT_ROOT"] = dirname(__DIR__);

require_once(dirname(__DIR__) . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

define('AUTH_BASIC', 'ros:patriot');
define('NEWS_PER_REQUEST', 1);
define('IBLOCK_ID', 1);
define('IBLOCK_SECTION_ID', false);
define('FORCE_UPDATE', false);
define('NEWS_PER_RUN', 5);

libxml_use_internal_errors(true);
$baseUrl = 'http://old.rospatriotcentr.ru';
$uploadDir = dirname(__DIR__) . '/upload/news/';
$tmpDir = dirname(__DIR__) . '/upload/tmp/news/';

if (!is_dir($uploadDir)) {
    mkdir($uploadDir, 0755, true);
}
if (!is_dir($tmpDir)) {
    mkdir($tmpDir, 0755, true);
}
$sectValue = '';

if (file_exists(__DIR__ . '/list.dat')) {
    $sectValuesRes = CIBlockPropertyEnum::GetList(
        ["SORT" => "ASC", "VALUE" => "ASC"],
        ['XML_ID' => 'main', 'IBLOCK_ID' => IBLOCK_ID]
    );
    if ($sectValue = $sectValuesRes->Fetch()) {
        $sectValue = $sectValue['ID'];
    }

    $list = unserialize(file_get_contents(__DIR__ . '/list.dat'));

    $list = array_filter($list, function ($item) {
        return !$item['done'];
    });

    $counter = 0;
    foreach ($list as &$item) {
        $newsRes = CIBlockElement::GetList(
            ["SORT" => "ASC"],
            [
                [
                    'LOGIC' => 'OR',
                    'NAME' => $item['title'],
                    'XML_ID' => 'import_' . $item['id'],
                ]
            ]
        );
        $existNewsId = null;
        if ($existsNews = $newsRes->Fetch()) {
            if (FORCE_UPDATE !== true) {
                $item['done'] = true;
                continue;
            }
            $existNewsId = $existsNews['ID'];
            unset($existsNews);
        }
        $news = httpClient::get($item['url']);
        $news = trim($news);
        /*
        $fh = fopen(__DIR__ . '/tmp.dat', 'wb');
        fwrite($fh, $news);
        fclose($fh);
        */
        $news = mb_convert_encoding($news, 'HTML-ENTITIES', "UTF-8");
        $doc = new DOMDocument();
        $doc->encoding = 'UTF-8';
        $doc->loadHTML($news);
        $xpath = new DOMXpath($doc);

        $mainImage = $xpath->query('//img[@class="mainImgOfArt"]');
        if ($mainImage->length > 0) {
            $src = $mainImage->item(0)->getAttribute('src');
            // print 'Main image: ' . $src . PHP_EOL;
            $name = $tmpDir . md5($src) . '.jpg';
            if (!file_exists($name)) {
                $image = httpClient::get($baseUrl . $src);
                $fh = fopen($name, 'wb');
                fwrite($fh, $image);
                fclose($fh);
                unset($image, $fh);
            }
            $item['image'] = $name;
            unset($src, $name);
        }
        unset($mainImage);
        $text = $xpath->query('//div[@class="fullText"]');
        if ($text->length > 0) {
            $htmlText = $text->item(0)->C14N();
            if (preg_match_all('#<img(.*)src="([^\"]+)#i', $htmlText, $innerImages)) {
                foreach ($innerImages[2] as $src) {
                    if (!preg_match('#^\/#', $src)) {
                        continue;
                    }
                    // print 'inline image: ' . $src . PHP_EOL;
                    $extension = pathinfo($src, PATHINFO_EXTENSION);
                    $name = $uploadDir . '/' . $item['id'] . '/' . md5($src) . '.' . ($extension ?: '.jpg');
                    if (!file_exists($name)) {
                        $image = httpClient::get($baseUrl . $src);

                        if (!is_dir($uploadDir . '/' . $item['id'])) {
                            mkdir($uploadDir . '/' . $item['id'], 0755);
                        }

                        $fh = fopen($name, 'wb');
                        fwrite($fh, $image);
                        fclose($fh);
                    }
                    $imageUrl = '/upload/news/' . $item['id'] . '/' . md5($src) . '.' . ($extension ?: '.jpg');
                    $htmlText = preg_replace('#src="' . preg_quote($src) . '#', 'src="' . $imageUrl, $htmlText);
                }
            }
            $htmlText = preg_replace('#\&\#xD;#', '', $htmlText);
            $item['text'] = $htmlText;
        }
        unset($text, $htmlText);
        $gallery = $xpath->query('//div[@class="artMediaSlider"]/div/a');
        if ($gallery->length > 0) {
            /** @var DOMElement $aElement */
            foreach ($gallery as $aElement) {
                $src = $aElement->getAttribute('href');
                // print 'gallery image: ' . $src . PHP_EOL;
                $extension = pathinfo($src, PATHINFO_EXTENSION);
                $name = $tmpDir . '/' . md5($src) . '.' . ($extension ?: '.jpg');
                if (!file_exists($name)) {
                    $image = httpClient::get($baseUrl . $src);
                    $fh = fopen($name, 'wb');
                    fwrite($fh, $image);
                    fclose($fh);
                }

                if (isset($item['gallery'])) {
                    array_push($item['gallery'], $name);
                } else {
                    $item['gallery'] = [$name];
                    if (!isset($item['image']) || empty($item['image'])) {
                        $item['image'] = $name;
                    }
                }
                unset($src, $name);
            }
        }
        unset($gallery);
        if ($existNewsId && FORCE_UPDATE && updateNews($existNewsId, $item)) {
            $item['done'] = true;
            unset($item['text']);
        } elseif (insertNews($item)) {
            $item['done'] = true;
            unset($item['text']);
        }
        $counter++;
        if ($counter >= NEWS_PER_RUN) {
            break;
        }
    }

    $fh = fopen(__DIR__ . '/list.dat', 'wb');
    fwrite($fh, serialize($list));
    fclose($fh);
    if (empty($list)) {
        die('done.');
    }
}

function insertNews($news)
{
    global $sectValue;
    $el = new CIBlockElement();


    if ($ID = $el->Add([
        'NAME' => $news['title'],
        'ACTIVE' => 'Y', // активен
        'ACTIVE_FROM' => date('d.m.Y', strtotime($news['date'])),
        'PREVIEW_TEXT' => $news['announce'],
        'IBLOCK_ID' => IBLOCK_ID,
        'IBLOCK_SECTION_ID' => IBLOCK_SECTION_ID,
        'MODIFIED_BY' => 1,
        'DETAIL_TEXT' => $news['text'],
        'DETAIL_TEXT_TYPE' => 'html',
        'PREVIEW_PICTURE' => $news['image'] ? CFile::MakeFileArray($news['image']) : '',
        'XML_ID' => 'import_' . $news['id'],
    ])) {
        // Раздел
        $el->SetPropertyValues($ID, IBLOCK_ID, $sectValue, 'SECT');

        // галерея
        if (isset($news['gallery']) && !empty($news['gallery'])) {
            $values = [];
            foreach ($news['gallery'] as $image) {
                array_push($values, CFile::MakeFileArray($image));
            }
            $el->SetPropertyValues($ID, IBLOCK_ID, $values, 'GALLERY');
        }

    } else {
        echo "Error: " . $el->LAST_ERROR;
    }

    return $ID;
}

function updateNews($id, $news)
{
    global $sectValue;
    $el = new CIBlockElement();


    if ($ID = $el->Update($id,
        [
            'NAME' => $news['title'],
            'ACTIVE' => 'Y', // активен
            'ACTIVE_FROM' => date('d.m.Y', strtotime($news['date'])),
            'PREVIEW_TEXT' => $news['announce'],
            'IBLOCK_ID' => IBLOCK_ID,
            'IBLOCK_SECTION_ID' => IBLOCK_SECTION_ID,
            'MODIFIED_BY' => 1,
            'DETAIL_TEXT' => $news['text'],
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_PICTURE' => $news['image'] ? CFile::MakeFileArray($news['image']) : '',
            'XML_ID' => 'import_' . $news['id'],
        ])) {
        // Раздел
        $el->SetPropertyValues($ID, IBLOCK_ID, $sectValue, 'SECT');

        // галерея
        if (isset($news['gallery']) && !empty($news['gallery'])) {
            $values = [];
            foreach ($news['gallery'] as $image) {
                array_push($values, CFile::MakeFileArray($image));
            }
            $el->SetPropertyValues($ID, IBLOCK_ID, $values, 'GALLERY');
        }

    } else {
        echo "Error: " . $el->LAST_ERROR;
    }

    return $ID;
}
