<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 17.09.19
 * Time: 17:17
 */

define('AUTH_BASIC', 'ros:patriot');
libxml_use_internal_errors(true);
$baseUrl = 'http://old.rospatriotcentr.ru';

ini_set('display_errors', 'On');
error_reporting(E_ALL);

$page = file_exists(__DIR__ . '/page.dat') ? (int)file_get_contents(__DIR__ . '/page.dat') : 1;

require('httpClient.php');

$response = httpClient::get($baseUrl .
    '/ajax.php?action=more-news&region=undefined&category=undefined&num=' . $page);

$json = json_decode($response, true);

$list = [];
if (file_exists(__DIR__ . '/list.dat')) {
    $list = unserialize(file_get_contents(__DIR__ . '/list.dat'));
}

if (isset($json['data']) &&
    !empty($json['data']) &&
    $page <= $json['pagesNum']) {
    foreach ($json['data'] as $date => $arrNews) {
        foreach ($arrNews as $news) {
            $news = trim($news);
            $news = mb_convert_encoding($news, 'HTML-ENTITIES', "UTF-8");
            $doc = new DOMDocument();
            $doc->encoding = 'UTF-8';
            $doc->loadHTML($news);
            $xpath = new DOMXpath($doc);
            $title = $xpath->query("//div/h2/a");
            $announce = $xpath->query("//div/a/p");

            $item = [];
            if ($title->length > 0) {
                $titleElement = $title->item(0);
                $item['title'] = trim($titleElement->textContent);
                $item['date'] = $date;
                $item['url'] = $baseUrl . $titleElement->getAttribute('href');
                $item['id'] = 0;
                if (preg_match('#\/([\d]+)#', $titleElement->getAttribute('href'), $matches)) {
                    $item['id'] = (int)$matches[1];
                }
                $item['done'] = false;
            }
            if ($announce->length > 0) {
                $item['announce'] = trim($announce->item(0)->textContent);
            }
            if ($item) {
                array_push($list, $item);
            }
        }
    }
    if ($list) {
        $fh = fopen(__DIR__ . '/list.dat', 'wb');
        fwrite($fh, serialize($list));
        fclose($fh);
    }
    $page++;
    $fh = fopen(__DIR__ . '/page.dat', 'wb');
    fwrite($fh, $page);
    fclose($fh);
   // print 'Page: ' . $page . ' OK' . PHP_EOL;
} else {
    $page++;
    $fh = fopen('page.dat', 'wb');
    fwrite($fh, $page);
    fclose($fh);
    die('done.');
}
