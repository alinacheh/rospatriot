<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 18.09.19
 * Time: 11:29
 */

class httpClient
{
    public static function get($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, AUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new Exception('curl Error: '. curl_error($ch), curl_errno($ch));
        }

        curl_close($ch);

        return $response;
    }
}
