<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 16.10.19
 * Time: 15:49
 */
use Bitrix\Main\Page\Asset;

AddEventHandler("main", "OnAdminTabControlBegin", "MyOnAdminTabControlBegin");
function MyOnAdminTabControlBegin()
{
    Asset::getInstance()->addJs('/local/php_interface/events/js/scripts.js');
}

AddEventHandler("main", "OnEpilog", "Redirect404");
function Redirect404() {
    if(
        !defined('ADMIN_SECTION') &&
        defined("ERROR_404")) {
        LocalRedirect("/", "404 Not Found");
    }
}
