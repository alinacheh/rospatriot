(function ($) {    
   $(".option .mediateca__link").click(function () {
     var text = $(this).parent().data('value');
     $("span .current").val(text);
   });
   $('body').on('click', '.header__search--close', function(){
     $('.header__search-form').removeClass('active');
     $('.header__search-form input').val('');
   })
   $('body').on('click', '.group-search__close', function(){
     $(this).closest('form').find('input').val('');
   });

   var frontSliders = $('.front-sliders');
  frontSliders.each(function(){
    var eventSwiper = new Swiper($(this).find(".swiper-container"), {
        slidesPerGroup: 1,
        spaceBetween: 10,
        slidesPerView: 4,
        breakpoints: {
            1100: {
                slidesPerView: 3,
            },
            900: {
                slidesPerView: 2,
            },
            767: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        },
        navigation: {
          nextEl: $(this).find('.front-sliders__right'),
          prevEl: $(this).find('.front-sliders__left'),
        },
    });
});


$(".years-project__toggle").click(function(){
	$('.years-project__more').slideToggle(300, function(){
		if ($(this).is(':hidden')) {
			$('.years-project__toggle').html('Загрузить ещё');
		} else {
			$('.years-project__toggle').html('Скрыть');
		}							
	});
	return false;
});

})(jQuery);
