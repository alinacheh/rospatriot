<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?= defined('IN_CONTENT') ? '</div><!-- /.document_content -->' : ''; // tag must be closed for index page
?>
<div class="document__footer">
    <!-- begin::footer -->
    <footer class="footer">
        <div class="footer__box">
            <a href="/" class="footer__logo">
                <img src="/local/templates/rospatriot/static/img/general/logo2.png" alt="">
            </a>
            <div class="footer__smlogo">
                <img src="/local/templates/rospatriot/static/img/general/smlogo.png" alt="">
            </div>
            <div class="footer__text">Федеральное государственное бюджетное учреждение «Российский центр гражданского и
                патриотического воспитания детей и молодежи»
            </div>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "bottom",
            [
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "Y",
                "MENU_CACHE_TIME" => "36000000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => [],
                "MAX_LEVEL" => 2,
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ],
            false
        ); ?>
        <div class="footer__box">
            <a href="https://fadm.gov.ru/" target="_blank" class="footer__sublogo">
                <img src="/local/templates/rospatriot/static/img/general/pmBig.svg" alt="">
            </a>
        </div>
    </footer>
    <!-- end::footer -->
</div>
</div><!-- /.document -->
<!-- begin::modal -->
<div class="modal" id="thanks" role="dialog" aria-labelledby="thanks" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Заголовок модального окна</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <svg class="icon__close" width="47.971px" height="47.971px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close"></use>
                    </svg>

                </button>
            </div>
            <div class="modal-body">
                Содержимое модального окна
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>

<div class="modal become" id="become" role="dialog" aria-labelledby="thanks" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Как стать волонтером?</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <svg class="icon__close" width="47.971px" height="47.971px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close"></use>
                    </svg>

                </button>
            </div>
            <div class="modal-body">
                <ol class="become__flex">
                    <li class="become__item">
                        <div class="become__icon">
                            <svg class="icon__reg" width="32px" height="32px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#reg"></use>
                            </svg>
                        </div>
                        <div class="become__text">Регистрируйся на портале
                            <br><a href="http://добровольцыроссии.рф">добровольцыроссии.рф</a>
                        </div>
                    </li>
                    <li class="become__item">
                        <div class="become__icon">
                            <svg class="icon__uch" width="32px" height="32px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#uch"></use>
                            </svg>
                        </div>
                        <div class="become__text">Изучай информацию о направлениях
                            <br>и видах волонтерства
                        </div>
                    </li>
                    <li class="become__item">
                        <div class="become__icon">
                            <svg class="icon__mapper" width="32px" height="32px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#mapper"></use>
                            </svg>
                        </div>
                        <div class="become__text">Выбирай свой регион и присоединяйся
                            <br>к проектам и организациям
                        </div>
                    </li>
                    <li class="become__item">
                        <div class="become__icon">
                            <svg class="icon__heart" width="32px" height="32px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#heart"></use>
                            </svg>
                        </div>
                        <div class="become__text">Стань частью большого сообщества
                            <br>волонтеров России
                        </div>
                    </li>
                </ol>
                <div class="become__double">
                    <div class="become__txt">Все разнообразие волонтерской деятельности в</div>
                    <a href="https://xn--90acesaqsbbbreoa5e3dp.xn--p1ai/" target="_blank" class="become__btn btn">ЕИС
                        «Добровольцы России»</a>
                </div>
            </div>
            <div class="become__boxs">
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc1.jpg)">
                    <div class="become__content">
                        <div class="become__border">Экологическое
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc2.jpg)">
                    <div class="become__content">
                        <div class="become__border">Медицинское
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc3.jpg)">
                    <div class="become__content">
                        <div class="become__border">Волонтерство
                            <br>в сфере патриотического
                            <br>воспитания
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc4.jpg)">
                    <div class="become__content">
                        <div class="become__border">Социальное
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc5.jpg)">
                    <div class="become__content">
                        <div class="become__border">Событийное
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc6.jpg)">
                    <div class="become__content">
                        <div class="become__border">Школьное
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc7.jpg)">
                    <div class="become__content">
                        <div class="become__border">«Серебряное»
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc8.jpg)">
                    <div class="become__content">
                        <div class="become__border">Волонтерство
                            <br>в сфере культуры
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc9.jpg)">
                    <div class="become__content">
                        <div class="become__border">Волонтерство
                            <br>в сфере образования
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc10.jpg)">
                    <div class="become__content">
                        <div class="become__border">Поиск людей</div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc11.jpg)">
                    <div class="become__content">
                        <div class="become__border">Инклюзивное
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc12.jpg)">
                    <div class="become__content">
                        <div class="become__border">Волонтерство
                            <br>в чрезвычайных
                            <br>ситуациях
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc13.jpg)">
                    <div class="become__content">
                        <div class="become__border">Корпоративное
                            <br>волонтерство
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc14.jpg)">
                    <div class="become__content">
                        <div class="become__border">Волонтерство
                            <br>в сфере формирования
                            <br>комфортной
                            <br>городской среды
                        </div>
                    </div>
                </div>
                <div class="become__box" style="background-image:url(/local/templates/rospatriot/static/img/general/desc15.jpg)">
                    <div class="become__content">
                        <div class="become__border">Медиаволонтерство</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52183354, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/52183354" style="position:absolute; left:-9999px;" alt=""/>
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150103445-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-150103445-1');
</script>

<div class="preloader">
    <div class="loader"></div>
</div>
</body>

</html>
