<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="prog-proj__flex">
        <? foreach ($arResult["ITEMS"] as $arItem):
            $eventStartDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"], CSite::GetDateFormat()) :
                MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat());
            $eventEndDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()) :
                $eventStartDateTimestamp;
            ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="prog-proj__box" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" data-toggle="modal" data-target="#<?= $this->GetEditAreaId($arItem['ID']); ?>-modal" style="background-image:url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)">
            <div class="prog-proj__cont">
                <div class="prog-proj__name"><?=$arItem["PROPERTIES"]["CATEGORY"]["VALUE"]; ?></div>
                <div class="prog-proj__content"><? echo $arItem["NAME"] ?></div>
            </div>
            </div>


<div class="modal event-modal" id="<?= $this->GetEditAreaId($arItem['ID']); ?>-modal" role="dialog" aria-labelledby="thanks" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="event-modal__social  event-modal__social--fix">
                        <a href="https://vk.com/hellomyrussia" target="_blank" class="event-modal-link">
                            <svg class="icon__vk-logo" width="548px" height="548px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#vk-logo"></use>
                            </svg>
                        </a>
                        <a href="https://www.facebook.com/rpatriotcentr/" target="_blank" class="event-modal-link">
                            <svg class="icon__facebook-logo" width="96px" height="96px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#facebook-logo"></use>
                            </svg>
                        </a>
                        <a href="https://twitter.com/RPatriotCenter" target="_blank" class="event-modal-link">
                            <svg class="icon__twitter-logo" width="612px" height="612px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#twitter-logo"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="modal-header">
                        <h2 class="modal-title"><? echo $arItem["NAME"] ?></h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg class="icon__close" width="47.971px" height="47.971px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="event-modal__row">
                            <div class="event-modal__date"><?=$arItem["PROPERTIES"]["CATEGORY"]["VALUE"]; ?></div>
                        </div>
                        <div class="event-modal__image">
                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
                        </div>
                        <div class="html">
                            <div class="detail_text"><?=$arItem["DETAIL_TEXT"]; ?></div>
                            <p style="margin-left: 40px;"><b>
                            <? if ($arItem["DISPLAY_PROPERTIES"]['ATT_LINK']){?> 
                                <?=$arItem['DISPLAY_PROPERTIES']['ATT_LINK']['NAME']?>  
                                <a href="<?= $arItem["PROPERTIES"]["ATT_LINK"]["VALUE"]?>"><?= $arItem["PROPERTIES"]["ATT_LINK"]["VALUE"]?></a>
                                <?}?> 
                            </b>
                            </p>
                            <p style="margin-left: 40px;"><strong><span style="color:#333333">
                                <? if ($arItem["DISPLAY_PROPERTIES"]['CONTACTS']){?> 
                                <?=$arItem['DISPLAY_PROPERTIES']['CONTACTS']['NAME']?>  
                                <?echo $arItem['DISPLAY_PROPERTIES']['CONTACTS']['VALUE'];?> 
                                <?}?> 
                            </span></strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? endforeach; ?>
</div>
