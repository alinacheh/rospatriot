<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="col-xl-4 col-lg-6 col-md-6">
		<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" class="card-news" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="card-news__image" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>)" alt=""></div>
			<div class="card-news__background"></div>
			<div class="card-news__content">
				<?if($arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']):?>
					<div class="card-news__category"><?=$arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']?></div>
				<?endif;?>
				<div class="card-news__share">
					<div class="card-news__share-icon">
						<svg class="icon__share-forward-line" width="24px" height="24px">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
						</svg>
					</div>
				</div>
				<div class="card-news__text">
					<div class="card-news__date">
						<?echo $arItem["DISPLAY_ACTIVE_FROM"];?>
						<div class="card-news__dot"></div>
						<?=$arItem['DISPLAY_PROPERTIES']['CATEGORY']['VALUE']?>
					</div>
					<div class="card-news__title"><?echo $arItem["NAME"];?></div>
				</div>
			</div>
		</a>
	</div>
<?endforeach;?>