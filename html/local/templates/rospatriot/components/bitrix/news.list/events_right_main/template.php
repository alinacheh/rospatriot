<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="swiper-container">
    <div class="swiper-wrapper">
        <? foreach ($arResult["ITEMS"] as $arItem):
            $eventStartDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"], CSite::GetDateFormat()) :
                MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat());
            $eventEndDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()) :
                $eventStartDateTimestamp;
            ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="piotica__box">
                    <div class="card-news__category">
                        События<? /*echo $arItem["DISPLAY_PROPERTIES"]["ATT_SECTION"]["VALUE"];*/
                        ?></div>
                    <? if (CIBlockFormatProperties::DateFormat("f", $eventStartDateTimestamp) ===
                        CIBlockFormatProperties::DateFormat("f", $eventEndDateTimestamp)): ?>
                        <div class="piotica__day"><?= CIBlockFormatProperties::DateFormat("j",
                                $eventStartDateTimestamp);
                            ?><?= $eventStartDateTimestamp != $eventEndDateTimestamp ? '-' . CIBlockFormatProperties::DateFormat("j",
                                    $eventEndDateTimestamp) : '' ?>
                        </div>
                        <div class="piotica__date"><?= CIBlockFormatProperties::DateFormat("F",
                                $eventStartDateTimestamp); ?></div>
                    <? else: ?>
                        <div class="piotica__day"><?= CIBlockFormatProperties::DateFormat("j F",
                                $eventStartDateTimestamp); ?> -
                        </div>
                        <div class="piotica__date"><?= CIBlockFormatProperties::DateFormat("j F",
                                $eventEndDateTimestamp); ?></div>
                    <? endif; ?>
                    <div class="piotica__subtitle"><? echo $arItem["NAME"]; ?></div>
                    <div class="front-sliders__region">
                        <?php if ($arItem["DISPLAY_PROPERTIES"]["ATT_PLACE"]["VALUE"]): ?>
                            <div class="front-sliders__icon">
                                <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                                </svg>
                            </div><?= $arItem["DISPLAY_PROPERTIES"]["ATT_PLACE"]["VALUE"]; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>
