<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<?php if ($arResult['ITEMS']): ?>
    <div class="sect-title">Новости партнеров</div>
    <div class="new">
        <div class="new-scroll">
            <?php foreach ($arResult['ITEMS'] as $item):
                $this->AddEditAction(
                    $item['ID'],
                    $item['EDIT_LINK'],
                    CIBlock::GetArrayByID(
                        $item["IBLOCK_ID"],
                        "ELEMENT_EDIT"
                    )
                );
                $this->AddDeleteAction(
                    $item['ID'],
                    $item['DELETE_LINK'],
                    CIBlock::GetArrayByID(
                        $item["IBLOCK_ID"],
                        "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                );
                ?>
                <div class="new-item" id="<?= $this->GetEditAreaId($item['ID']); ?>">
                    <div class="new-date"><?= $item['DISPLAY_ACTIVE_FROM']; ?></div>
                    <a href="<?= $item['DETAIL_PAGE_URL']; ?>" onclick="" class="new-title"><?= $item['NAME'] ?></a>
                    <div class="new-text"><?= $item['PREVIEW_TEXT']; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="new-footer">
            <a href="/news/partner/" class="btn btn-base" onclick="">
                <div class="btn-text">все новости партнеров</div>
            </a>
        </div>
    </div>
<?php endif; ?>
