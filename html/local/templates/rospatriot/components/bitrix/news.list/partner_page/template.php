<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="news new">
	<div class="header"></div>
	<div class="container">
		<div class="mediateca__header">
			<div class="page-title">Новости партнеров</div>
		</div>
	</div>
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	<?$i = 0?>
	<div class="news-innerx">
		<div class="container-full">
			<div class="row no-gutters news-list">
	
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		$i++;
		?>
		<?if($i == 1 || $i == 14):?>
			<div class="col-xl-6 news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" onclick="" class="card-news" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="card-news__image" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt=""></div>
					<div class="card-news__background"></div>
					<div class="card-news__content">
						<?if($arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']):?>
							<div class="card-news__category"><?=$arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']?></div>
						<?endif;?>
						<div class="card-news__share">
							<div class="card-news__share-icon">
								<svg class="icon__share-forward-line" width="24px" height="24px">
									<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
								</svg>
							</div>
						</div>
						<div class="card-news__text">
							<div class="card-news__date">
								<?echo $arItem["DISPLAY_ACTIVE_FROM"];?>
								<div class="card-news__dot"></div>
								Новости партнеров
							</div>
							<div class="card-news__title"><?echo $arItem["NAME"];?></div>
							<div class="new-text"><?= $arItem['PREVIEW_TEXT']; ?></div>
						</div>
					</div>
				</a>
			</div>
			<?if($i == 14){$i=0;}?>
		<?else:?>
			<div class="col-xl-3 news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" onclick="" class="card-news card-news-bg" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="card-news__image" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
						<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
					</div>
					<div class="card-news__background"></div>
					<div class="card-news__content">
						<?if($arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']):?>
							<div class="card-news__category"><?=$arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']?></div>
						<?endif;?>
						<div class="card-news__share">
							<div class="card-news__share-icon">
								<svg class="icon__share-forward-line" width="24px" height="24px">
									<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
								</svg>
							</div>
						</div>
						<div class="card-news__text">
							<div class="card-news__date">
								<?echo $arItem["DISPLAY_ACTIVE_FROM"];?>
								<div class="card-news__dot"></div>
								Новости партнеров
							</div>
							<div class="card-news__title"><?echo $arItem["NAME"];?></div>
							<div class="new-text"><?= $arItem['PREVIEW_TEXT']; ?></div>
						</div>
					</div>
				</a>
			</div>
		<?endif;?>
	<?endforeach;?>
			</div>
		</div>
	</div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br><?=$arResult["NAV_STRING"]?>
	<?endif;?>

</div>
