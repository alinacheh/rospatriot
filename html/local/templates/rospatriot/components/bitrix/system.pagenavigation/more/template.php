<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
	return;
}
$this->createFrame()->begin("Загрузка навигации");
?>

		<?if($arResult["NavPageCount"] > 1):?>
		
			<?if ($arResult["NavPageNomer"]+1 <= $arResult["nEndPage"]):?>
				<?
					$plus = $arResult["NavPageNomer"]+1;
					$url = $arResult["sUrlPathParams"] . "PAGEN_".$arResult["NavNum"]."=".$plus;
		
				?>

				<div class="news__footx load_more" data-url="<?=$url?>">
					<div class="pagination">
						<div class="pagination__buttons">
							<?if ($arResult["NavPageNomer"] > 1) {?>
								<?if ($arResult["NavPageNomer"] > 2) {?>
									<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
										<svg class="icon__rg" width="24px" height="26.3px">
											<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
										</svg>
									</a>
								<?} else {?>
									<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
										<svg class="icon__rg" width="24px" height="26.3px">
											<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
										</svg>
									</a>
								<?}?>
							<?} else { // Если страница первая?>
								<span class="paginationPrevNext pagination__arrow pagination__box disabled">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</span>
							<?}?>
							<?$page = $arResult["nStartPage"]?>
							<?while($page <= $arResult["nEndPage"]) {?>
								<?if ($page == $arResult["NavPageNomer"]) {?>
									<span class="paginationCurrent pagination__box active"><?=$page?></span>
								<?} else {?>
									<a class="paginationPage pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page?>"><?=$page?></a>
								<?}?>
								<?$page++;?>
							<?}?>
							<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {?>
								<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</a>
							<?} else { // Если страница последняя ?>
								<span class="paginationPrevNext pagination__arrow pagination__box disabled">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</span>
							<?}?>
						</div>

						<div class="pagination__more" data-url="<?=$url?>">
							<a href="javascript:void(0)" class="btn btn--base">Загрузить еще</a>
						</div>
					</div>
				</div>
		
			<?else:?>

				<div class="news__footx load_more">
					<div class="pagination">
						<div class="pagination__buttons">
							<?if ($arResult["NavPageNomer"] > 1) {?>
								<?if ($arResult["NavPageNomer"] > 2) {?>
									<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
										<svg class="icon__rg" width="24px" height="26.3px">
											<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
										</svg>
									</a>
								<?} else {?>
									<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
										<svg class="icon__rg" width="24px" height="26.3px">
											<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
										</svg>
									</a>
								<?}?>
							<?} else { // Если страница первая?>
								<span class="paginationPrevNext pagination__arrow pagination__box disabled">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</span>
							<?}?>
							<?$page = $arResult["nStartPage"]?>
							<?while($page <= $arResult["nEndPage"]) {?>
								<?if ($page == $arResult["NavPageNomer"]) {?>
									<span class="paginationCurrent pagination__box active"><?=$page?></span>
								<?} else {?>
									<a class="paginationPage pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page?>"><?=$page?></a>
								<?}?>
								<?$page++;?>
							<?}?>
							<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {?>
								<a class="paginationPrevNext pagination__arrow pagination__box" onclick="" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</a>
							<?} else { // Если страница последняя ?>
								<span class="paginationPrevNext pagination__arrow pagination__box disabled">
									<svg class="icon__rg" width="24px" height="26.3px">
										<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
									</svg>
								</span>
							<?}?>
						</div>

						<div class="pagination__more">
							<a class="btn btn--base">Загружено всё</a>
						</div>
					</div>
				</div>

			<?endif?>
<?endif?>