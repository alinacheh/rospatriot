$(document).ready(function(){

    $(document).on('click', '.show-more-wrapper', function(){

        var targetContainer = $('.shop-cards-box'),          //  Контейнер, в котором хранятся элементы
            url =  $('.show-more-wrapper').attr('data-url');    //  URL, из которого будем брать элементы

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.show-more-wrapper').remove();

                    var elements = $(data).find('.shop-card'),  //  Ищем элементы
                        pagination = $(data).find('.show-more-wrapper');//  Ищем навигацию

                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    targetContainer.closest('.container-full').append(pagination); //  добавляем навигацию следом

                }
            })
        }

    });

});