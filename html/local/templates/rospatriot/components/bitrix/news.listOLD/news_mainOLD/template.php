<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 19.08.19
 * Time: 16:39
 */

/** @var array $arResult */
/** @var array $arParams */
$firstRow = array_splice($arResult['ITEMS'], 0, 2);
$secondRow = array_splice($arResult['ITEMS'], 0, 2);

?>
<section class="front-section">
    <div class="container-full">
        <div class="row no-gutters">
            <div class="col-xl-6 col-lg-12">
                <div class="banner-slider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="/social-activity/" class="banner"
                                   style="background-image: url(/local/templates/rospatriot/static/img/general/banner1.jpg);">
                                    <img src="/local/templates/rospatriot/static/img/general/banner1.jpg" alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="/social-activity/" class="banner"
                                   style="background-image: url(/local/templates/rospatriot/static/img/general/banner2.jpg);">
                                    <img src="/local/templates/rospatriot/static/img/general/banner2.jpg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="banner-slider-pagination"></div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-12">
                <div class="row no-gutters">
                    <?php foreach ($firstRow as $item):
                        $this->AddEditAction(
                            $item['ID'],
                            $item['EDIT_LINK'],
                            CIBlock::GetArrayByID(
                                $item["IBLOCK_ID"],
                                "ELEMENT_EDIT"
                            )
                        );
                        $this->AddDeleteAction(
                            $item['ID'],
                            $item['DELETE_LINK'],
                            CIBlock::GetArrayByID(
                                $item["IBLOCK_ID"],
                                "ELEMENT_DELETE"),
                            array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                        );
                        $tag = $item['DISPLAY_PROPERTIES']['TAG']['VALUE'] ?? '';
                        $category = $item['DISPLAY_PROPERTIES']['CATEGORY']['VALUE'] ?? ''; ?>
                        <div class="col-xl-6 col-lg-6 col-md-6">
                            <a href="<?= $item['DETAIL_PAGE_URL']; ?>" id="<?= $this->GetEditAreaId($item['ID']); ?>"
                               class="card-news">
                                <?php if ($item['PREVIEW_PICTURE']): ?>
                                    <div class="card-news__image"
                                         style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC']; ?>')"></div>
                                <?php endif; ?>
                                <div class="card-news__background"></div>
                                <div class="card-news__content">
                                    <?php if ($tag): ?>
                                        <div class="card-news__category"><?= $tag ?></div>
                                    <?php endif; ?>
                                    <div class="card-news__share">
                                        <div class="card-news__share-icon">
                                            <svg class="icon__share-forward-line" width="24px" height="24px">
                                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-news__text">
                                        <div class="card-news__date">
                                            <?= $item['DISPLAY_ACTIVE_FROM']; ?>
                                            <div class="card-news__dot"></div>
                                            <?= $category; ?>
                                        </div>
                                        <div class="card-news__title">
                                            <?= $item['NAME']; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($secondRow): ?>
                <div class="col-xl-6 col-lg-12">
                    <div class="row no-gutters">
                        <?php foreach ($secondRow as $item):
                            $this->AddEditAction(
                                $item['ID'],
                                $item['EDIT_LINK'],
                                CIBlock::GetArrayByID(
                                    $item["IBLOCK_ID"],
                                    "ELEMENT_EDIT"
                                )
                            );
                            $this->AddDeleteAction(
                                $item['ID'],
                                $item['DELETE_LINK'],
                                CIBlock::GetArrayByID(
                                    $item["IBLOCK_ID"],
                                    "ELEMENT_DELETE"),
                                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                            );
                            $tag = $item['DISPLAY_PROPERTIES']['TAG']['VALUE'] ?? '';
                            $category = $item['DISPLAY_PROPERTIES']['CATEGORY']['VALUE'] ?? ''; ?>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <a href="<?= $item['DETAIL_PAGE_URL']; ?>"
                                   id="<?= $this->GetEditAreaId($item['ID']); ?>" class="card-news">
                                    <?php if ($item['PREVIEW_PICTURE']): ?>
                                        <div class="card-news__image"
                                             style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC']; ?>')"></div>
                                    <?php endif; ?>
                                    <div class="card-news__background"></div>
                                    <div class="card-news__content">
                                        <?php if ($tag): ?>
                                            <div class="card-news__category"><?= $tag ?></div>
                                        <?php endif; ?>
                                        <div class="card-news__share">
                                            <div class="card-news__share-icon">
                                                <svg class="icon__share-forward-line" width="24px" height="24px">
                                                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="card-news__text">
                                            <div class="card-news__date">
                                                <?= $item['DISPLAY_ACTIVE_FROM']; ?>
                                                <div class="card-news__dot"></div>
                                                <?= $category; ?>
                                            </div>
                                            <div class="card-news__title">
                                                <?= $item['NAME']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-xl-6 col-lg-12">
                <div class="banner-one">
                    <a href="/patriotic/" class="banner"
                       style="background-image: url(/local/templates/rospatriot/static/img/general/banner2.jpg);">
                        <img src="/local/templates/rospatriot/static/img/general/banner2.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="front-sliders">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">6-23 августа</div>
                    <div class="front-sliders__text">Северо-Кавказский молодежный форум «Машук-2019»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Пятигорск
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">13-16 августа</div>
                    <div class="front-sliders__text">Окружной форум добровольцев ДФО, СФО, УФО «Добро за Уралом»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Новосибирская область
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">Июль - сентябрь</div>
                    <div class="front-sliders__text">Всероссийский конкурс «Регион добрых дел»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        вся Россия
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">20-26 августа</div>
                    <div class="front-sliders__text">Фестиваль творческих сообществ «Таврида – АРТ»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Республика Крым
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">4-6 сентября</div>
                    <div class="front-sliders__text">Секция по добровольчеству в рамках Восточного экономического
                        форума
                    </div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Владивосток
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">17-20 сентября</div>
                    <div class="front-sliders__text">Окружной форум добровольцев ПФО и ЮФО «Добро на юге России»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Ростовская область
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">24-27 сентября</div>
                    <div class="front-sliders__text">Окружной форум добровольцев ЦФО «Добро в сердце России»</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Воронежская область
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">3-7 октября</div>
                    <div class="front-sliders__text">Международный форум волонтеров-медиков</div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Ивановская область
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">Ноябрь</div>
                    <div class="front-sliders__text">Народное голосование в рамках Всероссийского конкурса «Доброволец
                        России – 2019»
                    </div>
                    <div class="front-sliders__region">
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="front-sliders__box">
                    <div class="front-sliders__title">5 декабря</div>
                    <div class="front-sliders__text">Международный форум добровольцев с торжественной церемонией
                        вручения премии «Доброволец России – 2019»
                    </div>
                    <div class="front-sliders__region">
                        <div class="front-sliders__icon">
                            <svg class="icon__map-pin-2-line" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
                            </svg>
                        </div>
                        Москва
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="front-section bg-gray front-section-news">
    <div class="container-full">
        <div class="row no-gutters">
            <div class="col-xl-3 front-section-news-aslide">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "partner_sidebar",
                    Array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["PARTNER_IBLOCK_ID"],
                        "NEWS_COUNT" => 2,//$arParams["NEWS_COUNT"],

                        "SORT_BY1" => $arParams["SORT_BY1"],
                        "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                        "SORT_BY2" => $arParams["SORT_BY2"],
                        "SORT_ORDER2" => $arParams["SORT_ORDER2"],

                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                        "CHECK_DATES" => $arParams["CHECK_DATES"],
                        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["partner"],
                        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["partner_detail"],
                        "SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],

                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

                        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                        "SET_TITLE" => 'N',
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "MESSAGE_404" => $arParams["MESSAGE_404"],
                        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                        "SHOW_404" => $arParams["SHOW_404"],
                        "FILE_404" => $arParams["FILE_404"],
                        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],

                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",

                        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                        "MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
                        "SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],

                        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                        "USE_RATING" => $arParams["USE_RATING"],
                        "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
                        "MAX_VOTE" => $arParams["MAX_VOTE"],
                        "VOTE_NAMES" => $arParams["VOTE_NAMES"],

                        "USE_SHARE" => $arParams["LIST_USE_SHARE"],
                        "SHARE_HIDE" => $arParams["SHARE_HIDE"],
                        "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
                        "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],

                        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                    ),
                    $component
                ); ?>
            </div>
            <div class="col-xl-9 front-section-news-content">
                <div class="row no-gutters">
                    <?php foreach ($arResult['ITEMS'] as $item):
                        $this->AddEditAction(
                            $item['ID'],
                            $item['EDIT_LINK'],
                            CIBlock::GetArrayByID(
                                $item["IBLOCK_ID"],
                                "ELEMENT_EDIT"
                            )
                        );
                        $this->AddDeleteAction(
                            $item['ID'],
                            $item['DELETE_LINK'],
                            CIBlock::GetArrayByID(
                                $item["IBLOCK_ID"],
                                "ELEMENT_DELETE"),
                            array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                        );
                        $tag = $item['DISPLAY_PROPERTIES']['TAG']['VALUE'] ?? '';
                        $category = $item['DISPLAY_PROPERTIES']['CATEGORY']['VALUE'] ?? '';
                        ?>
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <a href="news28.html" class="card-news">
                                <?php if ($item['PREVIEW_PICTURE']): ?>
                                    <div class="card-news__image"
                                         style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC']; ?>')"></div>
                                <?php endif; ?>
                                <div class="card-news__background"></div>
                                <div class="card-news__content">
                                    <?php if ($tag): ?>
                                        <div class="card-news__category"><?= $tag ?></div>
                                    <?php endif; ?>
                                    <div class="card-news__share">
                                        <div class="card-news__share-icon">
                                            <svg class="icon__share-forward-line" width="24px" height="24px">
                                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-news__text">
                                        <div class="card-news__date">
                                            <?= $item['DISPLAY_ACTIVE_FROM']; ?>
                                            <div class="card-news__dot"></div>
                                            <?= $category; ?>
                                        </div>
                                        <div class="card-news__title">
                                            <?= $item['NAME']; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>

                    <div class="col-xl-4 col-lg-6 col-md-6 d-flex">
                        <a href="/news/" class="full-link">
                            <div class="full-link__link">
                                <div class="full-link__text">Смотреть все</div>
                                <div class="full-link__icon">
                                    <svg class="icon__ar-left" width="24px" height="26.3px">
                                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#ar-left"></use>
                                    </svg>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
