<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$isAjax = isset($_REQUEST['IS_AJAX']) && $_REQUEST['IS_AJAX'] = 'Y' || (int)$arResult['NavPageNomer'] > 1;

$list = (array)$arResult['ITEMS'];
if (!$isAjax):
$firstRow = array_splice($list, 0, 5);

?>

<section class="front-section bg-gray front-section-news">
    <div class="container-full">
        <div class="row no-gutters">
            <div class="col-xl-3 front-section-news-aslide">

                <?$GLOBALS["partnerFilter"] = array("PROPERTY_SECT_VALUE"=>"Новости партнеров");
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"partner_sidebar", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/news/#ELEMENT_ID#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "partnerFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "CATEGORY",
			1 => "TAG",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ID",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "partner_sidebar"
	),
	false
);?>
            </div>
            <div class="col-xl-9 front-section-news-content">
                <div class="row no-gutters">
                    <?php foreach ($firstRow as $key => $item):
                            $longNews = $key < 1;
                            $this->AddEditAction(
                                $item['ID'],
                                $item['EDIT_LINK'],
                                CIBlock::GetArrayByID(
                                    $item["IBLOCK_ID"],
                                    "ELEMENT_EDIT"
                                )
                            );
                            $this->AddDeleteAction(
                                $item['ID'],
                                $item['DELETE_LINK'],
                                CIBlock::GetArrayByID(
                                    $item["IBLOCK_ID"],
                                    "ELEMENT_DELETE"),
                                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                            );
                            $tag = $item['DISPLAY_PROPERTIES']['TAG']['VALUE'] ?? '';
                            $category = $item['DISPLAY_PROPERTIES']['CATEGORY']['VALUE'] ?? '';
                            ?>
                    <div class="col-xl-<?= $longNews ? '8' : '4'; ?> col-lg-6 col-md-6"
                        id="<?= $this->GetEditAreaId($item['ID']); ?>">
                        <a href="<?= $item['DETAIL_PAGE_URL']; ?>"
                            class="card-news<?= $longNews ? '' : ' card-news-bg'; ?>">
                            <?php if ($item['PREVIEW_PICTURE']): ?>
                            <div class="card-news__image"
                                style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC']; ?>')">
                                <img src="<?= $item['PREVIEW_PICTURE']['SRC']; ?>"
                                    alt="<?= $item['PREVIEW_PICTURE']['ALT']; ?>">
                            </div>
                            <?php endif; ?>
                            <div class="card-news__background"></div>
                            <div class="card-news__content">
                                <?php if ($tag): ?>
                                <div class="card-news__category"><?= $tag; ?></div>
                                <?php endif; ?>
                                <div class="card-news__share">
                                    <div class="card-news__share-icon">
                                        <svg class="icon__share-forward-line" width="24px" height="24px">
                                            <use
                                                xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line">
                                            </use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="card-news__text">
                                    <div class="card-news__date">
                                        <?= $item['DISPLAY_ACTIVE_FROM']; ?>
                                        <div class="card-news__dot"></div>
                                        <?= $category; ?>
                                    </div>
                                    <div class="card-news__title">
                                        <?= $item['NAME'] ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<div class="news-innerx">
    <div class="container-full">
        <div class="row no-gutters">
            <?php foreach ($list as $key => $item):
                    $longNews = ($key - 6) % 7 == 0; // каждая 7я длинная
                    $this->AddEditAction(
                        $item['ID'],
                        $item['EDIT_LINK'],
                        CIBlock::GetArrayByID(
                            $item["IBLOCK_ID"],
                            "ELEMENT_EDIT"
                        )
                    );
                    $this->AddDeleteAction(
                        $item['ID'],
                        $item['DELETE_LINK'],
                        CIBlock::GetArrayByID(
                            $item["IBLOCK_ID"],
                            "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
                    );
                    $tag = $item['DISPLAY_PROPERTIES']['TAG']['VALUE'] ?? '';
                    $category = $item['DISPLAY_PROPERTIES']['CATEGORY']['VALUE'] ?? '';
                    ?>
            <div class="col-xl-<?= $longNews ? '6' : '3'; ?>">
                <a href="<?= $item['DETAIL_PAGE_URL']; ?>" id="<?= $this->GetEditAreaId($item['ID']); ?>"
                    class="card-news<?= $longNews ? '' : ' card-news-bg'; ?>">
                    <?php if ($item['PREVIEW_PICTURE']): ?>
                    <div class="card-news__image"
                        style="background-image: url('<?= $item['PREVIEW_PICTURE']['SRC']; ?>')">
                        <img src="<?= $item['PREVIEW_PICTURE']['SRC']; ?>"
                            alt="<?= $item['PREVIEW_PICTURE']['ALT']; ?>">
                    </div>
                    <?php endif; ?>
                    <div class="card-news__background"></div>
                    <div class="card-news__content">
                        <?php if ($tag): ?>
                        <div class="card-news__category"><?= $tag; ?></div>
                        <?php endif; ?>
                        <div class="card-news__share">
                            <div class="card-news__share-icon">
                                <svg class="icon__share-forward-line" width="24px" height="24px">
                                    <use
                                        xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line">
                                    </use>
                                </svg>
                            </div>
                        </div>
                        <div class="card-news__text">
                            <div class="card-news__date">
                                <?= $item['DISPLAY_ACTIVE_FROM']; ?>
                                <div class="card-news__dot"></div>
                                <?= $category; ?>
                            </div>
                            <div class="card-news__title">
                                <?= $item['NAME']; ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php if (!$isAjax): ?>
<?php endif; ?>