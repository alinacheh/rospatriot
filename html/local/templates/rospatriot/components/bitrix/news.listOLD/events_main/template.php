<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="swiper-container">
	<div class="swiper-wrapper">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="front-sliders__box">
				<?if(CIBlockFormatProperties::DateFormat("f", MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat())) === CIBlockFormatProperties::DateFormat("f", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()))):?>
					<div class="front-sliders__title"><?echo CIBlockFormatProperties::DateFormat("j", MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));?>-<?echo CIBlockFormatProperties::DateFormat("j", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()));?> <?echo CIBlockFormatProperties::DateFormat("F", MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));?></div>
				<?else:?>
					<div class="front-sliders__title"><?echo CIBlockFormatProperties::DateFormat("j F", MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));?> - <?echo CIBlockFormatProperties::DateFormat("j F", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()));?></div>
				<?endif;?>
				<div class="front-sliders__text"><?echo $arItem["NAME"];?></div>
				<div class="front-sliders__region">
					<div class="front-sliders__icon">
						<svg class="icon__map-pin-2-line" width="24px" height="24px">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#map-pin-2-line"></use>
						</svg>
					</div><?echo $arItem["DISPLAY_PROPERTIES"]["ATT_PLACE"]["VALUE"];?>
				</div>
			</div>
		</div>
	<?endforeach;?>
	</div>
</div>