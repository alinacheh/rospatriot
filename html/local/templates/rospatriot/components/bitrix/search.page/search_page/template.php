<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="doc-page">
	<div class="container">
		<div class="doc-page__header">
			<div class="title-page">Поиск</div>
			<div class="doc-page__controller">
				<?if($arResult["REQUEST"]["HOW"]=="d"):?>
					<a href="<?=$arResult["URL"]?>&amp;how=r" class="doc-page__link"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a>
					<a href="javascript:void(0)" class="doc-page__link active"><?=GetMessage("SEARCH_SORTED_BY_DATE")?></a>
				<?else:?>
					<a href="javascript:void(0)" class="doc-page__link active"><?=GetMessage("SEARCH_SORTED_BY_RANK")?></a>
					<a href="<?=$arResult["URL"]?>&amp;how=d" class="doc-page__link"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
				<?endif;?>
			</div>
		</div>
	
		<div class="doc-page__search">
			<div class="group-search w-100">
				<form action="" method="get">
					<?$APPLICATION->IncludeComponent(
						"bitrix:search.suggest.input",
						"",
						array(
							"NAME" => "q",
							"VALUE" => $arResult["REQUEST"]["~QUERY"],
							"INPUT_SIZE" => 40,
							"DROPDOWN_SIZE" => 10,
							"FILTER_MD5" => $arResult["FILTER_MD5"],
						),
						$component
					);?>
				<?if($arParams["SHOW_WHERE"]):?>
					&nbsp;<select name="where">
					<option value=""><?=GetMessage("SEARCH_ALL")?></option>
					<?foreach($arResult["DROPDOWN"] as $key=>$value):?>
					<option value="<?=$key?>"<?if($arResult["REQUEST"]["WHERE"]==$key) echo " selected"?>><?=$value?></option>
					<?endforeach?>
					</select>
				<?endif;?>
					<div class="group-search__close">
						<svg class="icon__search-2-line">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close-line"></use>
						</svg>
					</div>
					<button class="group-search__btn" type="submit" value="<?=GetMessage("SEARCH_GO")?>">
						<span>
							<svg class="icon__search-2-line" width="24px" height="24px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
							</svg>
						</span>
					</button>
					<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
				<?if($arParams["SHOW_WHEN"]):?>
					<script>
					var switch_search_params = function()
					{
						var sp = document.getElementById('search_params');
						var flag;
						var i;
				
						if(sp.style.display == 'none')
						{
							flag = false;
							sp.style.display = 'block'
						}
						else
						{
							flag = true;
							sp.style.display = 'none';
						}
				
						var from = document.getElementsByName('from');
						for(i = 0; i < from.length; i++)
							if(from[i].type.toLowerCase() == 'text')
								from[i].disabled = flag;
				
						var to = document.getElementsByName('to');
						for(i = 0; i < to.length; i++)
							if(to[i].type.toLowerCase() == 'text')
								to[i].disabled = flag;
				
						return false;
					}
					</script>
					<a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
					<div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
						<?$APPLICATION->IncludeComponent(
							'bitrix:main.calendar',
							'',
							array(
								'SHOW_INPUT' => 'Y',
								'INPUT_NAME' => 'from',
								'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
								'INPUT_NAME_FINISH' => 'to',
								'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
								'INPUT_ADDITIONAL_ATTR' => 'size="10"',
							),
							null,
							array('HIDE_ICONS' => 'Y')
						);?>
					</div>
				<?endif;?>
				</form>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="news-list">
			<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):?>
				<div class="search-language-guess">
					<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
				</div><br />
			<?endif;?>
			<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
			<?elseif($arResult["ERROR_CODE"]!=0):?>
				<?ShowError($arResult["ERROR_TEXT"]);?>
			<?elseif(count($arResult["SEARCH"])>0):?>

				<?if($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"]?>

				<?foreach($arResult["SEARCH"] as $arItem):?>
					<?if($arItem["PARAM2"] == 7 || $arItem["PARAM2"] == 6):?>
						<div class="doc__item news-item">
							<div class="doc-flex doc-sb">
								<?if($arItem["PARAM2"] == 7):?>
									<?if(CModule::IncludeModule("iblock")){
										$arSelect = array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_FILE", "PROPERTY_ATT_TYPE");
										$arFilter = array("IBLOCK_ID"=>$arItem["PARAM2"], "ID"=>$arItem["ITEM_ID"], "ACTIVE"=>"Y");
										$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
										while($arFields = $res->GetNext()){
											$file = CFile::GetFileArray($arFields["PROPERTY_ATT_FILE_VALUE"]);
											$size = $file["FILE_SIZE"];
											$sizes = array('Б', 'КБ', 'МБ', 'ГБ', 'ТБ', 'Pb', 'Eb', 'Zb', 'Yb');
											for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024;
											if($i > 1):
												$round = 2;
											else:
												$round = 0;
											endif;
											$size = round($size,$round)." ".$sizes[$i];
											$exten = $file["FILE_NAME"];
											$extension = mb_substr($exten, mb_strrpos($exten, '.')+1);?>
											<div>
												<div class="doc__name"><?echo $arItem["TITLE"];?></div>
												<div class="doc-flex">
													<div class="doc__type"><?echo $arFields["PROPERTY_ATT_TYPE_VALUE"];?></div>
													<?$ts = strtotime($arItem["DATE_FROM"]);?>
													<div class="doc__date">Добавлен: <?echo CIBlockFormatProperties::DateFormat("j F Y", $ts);?></div>
												</div>
											</div>
											<div class="doc__file">
												<a download="" href="<?echo $file["SRC"];?>" class="doc__download">
													<span class="icon">
														<svg class="icon__file-download-r" width="24px" height="24px">
															<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
														</svg>
													</span>
												</a>
												<div class="doc__size"><?echo mb_strtoupper($extension);?>, <?echo $size;?></div>
											</div>
										<?
										}
									}?>
								<?else:?>
									<?if(CModule::IncludeModule("iblock")){
										$arSelect = array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "DATE_ACTIVE_FROM", "PROPERTY_FORMAT", "PROPERTY_ATT_VIDEO", "PROPERTY_ATT_PHOTO");
										$arFilter = array("IBLOCK_ID"=>$arItem["PARAM2"], "ID"=>$arItem["ITEM_ID"], "ACTIVE"=>"Y");
										$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
										while($arFields = $res->GetNext()){?>
											<div>
												<div class="doc__name"><?echo $arItem["TITLE"];?></div>
												<div class="doc-flex">
													<?$res = CIBlock::GetByID($arItem["PARAM2"]);?>
													<?$ar_res = $res->GetNext();?>
													<div class="doc__type"><?echo $ar_res["NAME"];?></div>
													<?$ts = strtotime($arItem["DATE_FROM"]);?>
													<div class="doc__date">Добавлен: <?echo CIBlockFormatProperties::DateFormat("j F Y", $ts);?></div>
												</div>
											</div>
											<?if($arFields["PROPERTY_FORMAT_VALUE"] == "Фотогалерея"){
												$preview = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);
											?>
												<div class="doc__file">
													<a href="javascript:;" data-fancybox="hello<?=$arItem["ITEM_ID"];?>" data-src="<?echo $preview["SRC"];?>" class="doc__download">
														<span class="icon">
															<svg class="icon__file-download-r" width="24px" height="24px">
																<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
															</svg>
														</span>
													</a>
													<?$count = 1;?>
													<?$props= CIBlockElement::GetProperty($arItem["PARAM2"], $arItem["ITEM_ID"], "sort", "asc", array("CODE" => "ATT_PHOTO"));
													while ($props_ar = $props->GetNext())
													{
														$count++;
														$file = CFile::GetFileArray($props_ar["VALUE"]);
														?>
															<a href="javascript:;" data-fancybox="hello<?=$arItem["ITEM_ID"];?>" data-src="<?=$file["SRC"]?>" style="display:none;"></a>
														<?
													}
													?>
													<div class="doc__size"><?=$count?> ФОТО</div>
												</div>
											<?} else {?>
												<div class="doc__file">
													<a href="javascript:;" data-fancybox="hello<?=$arItem["ITEM_ID"];?>" data-src="<?echo $arFields["PROPERTY_ATT_VIDEO_VALUE"];?>" class="doc__download">
														<span class="icon">
															<svg class="icon__play" width="14px" height="17px">
																<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#play"></use>
															</svg>
														</span>
													</a>
													<div class="doc__size">СМОТРЕТЬ ВИДЕО</div>
												</div>
											<?
											}
										}
									}?>
								<?endif;?>
							</div>
						</div>
					<?else:?>
					<div class="doc__item news-item">
						<div class="doc-flex doc-sb">
							<div class="main-search-item">
								<?if($arItem["PARAM2"] == 1):?>
									<a href="/news/<?=$arItem["ITEM_ID"]?>/" class="doc__name"><?echo $arItem["TITLE"];?></a>
								<?elseif($arItem["PARAM2"] == 8):?>
									<a href="/smi/announcement/<?=$arItem["ITEM_ID"]?>/" class="doc__name"><?echo $arItem["TITLE"];?></a>
								<?else:?>
									<div class="doc__name"><?echo $arItem["TITLE"];?></div>
								<?endif;?>
								<div class="doc-flex">
									<?$res = CIBlock::GetByID($arItem["PARAM2"]);?>
									<?$ar_res = $res->GetNext();?>
									<div class="doc__type"><?echo $ar_res["NAME"];?></div>
									<?$ts = strtotime($arItem["DATE_FROM"]);?>
									<?if($arItem["PARAM2"] == 4 || $arItem["PARAM2"] == 8):?>
										<?if(CModule::IncludeModule("iblock")){
										$arSelect = array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_START", "PROPERTY_ATT_END");
										$arFilter = array("IBLOCK_ID"=>$arItem["PARAM2"], "ID"=>$arItem["ITEM_ID"], "ACTIVE"=>"Y");
										$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
										while($arFields = $res->GetNext()){
											$eventStartDateTimestamp = isset($arFields["PROPERTY_ATT_START_VALUE"]) ?
												MakeTimeStamp($arFields["PROPERTY_ATT_START_VALUE"], CSite::GetDateFormat()) :
												MakeTimeStamp($arFields["DATE_ACTIVE_FROM"], CSite::GetDateFormat());
											$eventEndDateTimestamp = isset($arFields["PROPERTY_ATT_END_VALUE"]) ?
												MakeTimeStamp($arFields["PROPERTY_ATT_END_VALUE"], CSite::GetDateFormat()) :
												$eventStartDateTimestamp;
											?>
											<? if (CIBlockFormatProperties::DateFormat("f", $eventStartDateTimestamp) ===
												CIBlockFormatProperties::DateFormat("f", $eventEndDateTimestamp)): ?>
												<div class="doc__date">
													<?= CIBlockFormatProperties::DateFormat("j", $eventStartDateTimestamp); ?><?=
														$eventStartDateTimestamp != $eventEndDateTimestamp ? '-' . CIBlockFormatProperties::DateFormat("j",
															$eventEndDateTimestamp) : ''; ?>
													<?= CIBlockFormatProperties::DateFormat("F Y", $eventStartDateTimestamp); ?></div>
											<? else: ?>
												<div class="doc__date">
													<?= CIBlockFormatProperties::DateFormat("j F", $eventStartDateTimestamp); ?>
													 - <? echo CIBlockFormatProperties::DateFormat("j F Y", $eventEndDateTimestamp); ?></div>
											<? endif; ?>
											<?
											}
										}?>
									<?else:?>
										<div class="doc__date">Добавлен: <?echo CIBlockFormatProperties::DateFormat("j F Y", $ts);?></div>
									<?endif;?>
								</div>
							</div>
						</div>
					</div>
					<?endif;?>
				<?endforeach;?>

				<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>

			<?else:?>
				<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
			<?endif;?>
		</div>
	</div>
</div>