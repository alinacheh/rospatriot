<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="partners">
    <div class="container">
        <div class="page-title">Партнеры</div>
        <div class="prog-box">
            <div class="prog-box__flex">
                <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
                <?= $arResult["NAV_STRING"] ?><br />
                <? endif; ?>
                <?$i = 0;?>
                <? foreach ($arResult["ITEMS"] as $arItem):
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
			CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
			CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
			array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

                <a class="prog-box__box" href="<?=$arItem["DISPLAY_PROPERTIES"]["ATT_URL"]["VALUE"]?>" target="_blank">
                    <div class="prog-box__visite">
                        <svg class="icon__arrow-right-up-line" width="24px" height="24px">
                            <use
                                xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#arrow-right-up-line">
                            </use>
                        </svg>
                    </div>
                    <div class="prog-box__image">
                        <img src="<? echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="
                        <? echo $arItem["NAME"];?>">
                    </div>
                    <div class="prog-box__content">
                        <? echo $arItem["NAME"];?>
                    </div>
                </a>
                <?
		$_POST["i"]++;
		if($_POST["i"] >= 4){$_POST["i"] = 0;}
		?>
                <? endforeach; ?>
                <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <br /><?= $arResult["NAV_STRING"] ?>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>
