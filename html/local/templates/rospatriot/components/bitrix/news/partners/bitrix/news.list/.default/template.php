<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult["ITEMS"]):?>

<div class="team__group open">
	<div class="team__btn" data-open="open">
		<?=$arResult["ITEMS"][0]["DISPLAY_PROPERTIES"]["ATT_SUBDIV"]["VALUE"]?>
	</div>
	<div class="team__section">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
	<?$depart = "";?>
	<div class="team__list">
		<? foreach ($arResult["ITEMS"] as $arItem):
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
				CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
				CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
				array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<?if($depart != $arItem["DISPLAY_PROPERTIES"]["ATT_DEPART"]["VALUE"]):?>
				</div>
				<div class="team__subtitle"><?=$arItem["DISPLAY_PROPERTIES"]["ATT_DEPART"]["VALUE"]?></div>
				<?$depart = $arItem["DISPLAY_PROPERTIES"]["ATT_DEPART"]["VALUE"];?>
				<div class="team__list">
			<?endif;?>
				<div class="team__item">
					<div class="team__image">
						<div class="team__image-bg<?if($_POST["i"] == 0){echo ' red-line-bg';}else if($_POST["i"] == 1){echo ' gray-line-bg';}else if($_POST["i"] == 2){echo ' red-bg';}else{echo ' white-bg';}?>"></div>
						<?if($arItem["PREVIEW_PICTURE"]["SRC"]):?>
							<img src="<? echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<? echo $arItem["NAME"];?>">
						<?endif;?>
					</div>
					<div class="team__name"><? echo $arItem["NAME"];?></div>
					<div class="team__status"><?=$arItem["DISPLAY_PROPERTIES"]["ATT_EMPLOYEE"]["VALUE"]?></div>
					<div class="team__mail">
						<a href="mailto:<?=$arItem["DISPLAY_PROPERTIES"]["ATT_EMAIL"]["VALUE"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["ATT_EMAIL"]["VALUE"]?></a>
					</div>
				</div>
			<?
			$_POST["i"]++;
			if($_POST["i"] >= 4){$_POST["i"] = 0;}
			?>
		<? endforeach; ?>
	</div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
	</div>
</div>
<?endif;?>