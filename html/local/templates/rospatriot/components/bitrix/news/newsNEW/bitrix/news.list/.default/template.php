<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
$Sect = array("Главные новости", "Региональные новости");
/*if($_GET['sort'] == "main" || empty($_GET['sort'])){
	$Sect = "Главные новости";
}else{
	$Sect = "Региональные новости";
}*/?>
<?
$Cate = "";
if ($_GET['cat'] == "d") {
	$Cate = "Добровольчество";
} else {
	if ($_GET['cat'] == "p") {
		$Cate = "Патриотика";
	}
} ?>

<?
$Tag = "";
if ($_GET['cat'] == "a") {
	$Tag = "Актуально";
} else {
	if ($_GET['tag'] == "y") {
		$Tag = "Дайджест";
	}
};
if ($_GET['tag'] == "y") {
	$Tag = "Дайджест";
} else {
	if ($_GET['tag'] == "v") {
		$Tag = "Добро в России";
	}
};
if ($_GET['tag'] == "v") {
	$Tag = "Добро в России";
} else {
	if ($_GET['tag'] == "i") {
		$Tag = "История недели";
	}
};
if ($_GET['tag'] == "i") {
	$Tag = "История недели";
} else {
	if ($_GET['tag'] == "l") {
		$Tag = "Люди говорят";
	}
};
if ($_GET['tag'] == "l") {
	$Tag = "Люди говорят";
} else {
	if ($_GET['tag'] == "o") {
		$Tag = "Официально";
	}
};
if ($_GET['tag'] == "o") {
	$Tag = "ОФициально";
} else {
	if ($_GET['tag'] == "t") {
		$Tag = "Патриотика";
	}
};
if ($_GET['tag'] == "t") {
	$Tag = "Патриотика";
} else {
	if ($_GET['tag'] == "u") {
		$Tag = "Принимай участие";
	}
};
if ($_GET['tag'] == "u") {
	$Tag = "Принимай участие";
} else {
	if ($_GET['tag'] == "s") {
		$Tag = "События недели";
	}
};
if ($_GET['tag'] == "s") {
	$Tag = "События недели";
} else {
	if ($_GET['tag'] == "a") {
		$Tag = "Актуально";
	}
};
?>

<?if(empty($_GET['PAGEN_2']) || $_GET['PAGEN_2'] == "1"):?>
<?$GLOBALS["newsFilter"] = array("PROPERTY_SECT_VALUE"=>$Sect, "PROPERTY_CATEGORY_VALUE"=>$Cate, "PROPERTY_TAG_VALUE"=>$Tag);?>
<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"news",
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/news/#ELEMENT_ID#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "",
		),
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "newsFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "CATEGORY",
			1 => "TAG",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "news"
	),
	false
);?>
<?endif;?>


<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<?$i = 0?>
<div class="news-innerx">
	<div class="container-full">
		<div class="row no-gutters news-list">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$i++;
	?>
	<?if($i == 1 || $i == 14):?>
		<div class="col-xl-6 news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" onclick="" class="card-news" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="card-news__image" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt=""></div>
				<div class="card-news__background"></div>
				<div class="card-news__content">
					<?if($arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']):?>
						<div class="card-news__category"><?=$arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']?></div>
					<?endif;?>
					<div class="card-news__share">
						<div class="card-news__share-icon">
							<svg class="icon__share-forward-line" width="24px" height="24px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
							</svg>
						</div>
					</div>
					<div class="card-news__text">
						<div class="card-news__date">
							<?echo $arItem["DISPLAY_ACTIVE_FROM"];?>
							<div class="card-news__dot"></div>
							<?=$arItem['DISPLAY_PROPERTIES']['CATEGORY']['VALUE']?>
						</div>
						<div class="card-news__title"><?echo $arItem["NAME"];?></div>
					</div>
				</div>
			</a>
		</div>
		<?if($i == 14){$i=0;}?>
	<?else:?>
		<div class="col-xl-3 news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" onclick="" class="card-news card-news-bg" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="card-news__image" style="background-image: url(<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
					<img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="">
				</div>
				<div class="card-news__background"></div>
				<div class="card-news__content">
					<?if($arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']):?>
						<div class="card-news__category"><?=$arItem['DISPLAY_PROPERTIES']['TAG']['VALUE']?></div>
					<?endif;?>
					<div class="card-news__share">
						<div class="card-news__share-icon">
							<svg class="icon__share-forward-line" width="24px" height="24px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#share-forward-line"></use>
							</svg>
						</div>
					</div>
					<div class="card-news__text">
						<div class="card-news__date">
							<?echo $arItem["DISPLAY_ACTIVE_FROM"];?>
							<div class="card-news__dot"></div>
							<?=$arItem['DISPLAY_PROPERTIES']['CATEGORY']['VALUE']?>
						</div>
						<div class="card-news__title"><?echo $arItem["NAME"];?></div>
					</div>
				</div>
			</a>
		</div>
	<?endif;?>
<?endforeach;?>
		</div>
	</div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br><?=$arResult["NAV_STRING"]?>
<?endif;?>
