<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arParams["USE_RSS"] == "Y"): ?>
    <?
    if (method_exists($APPLICATION, 'addheadstring')) {
        $APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" href="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" />');
    }
    ?>
<? endif ?>
<?
$Sect = array("Главные новости", "Региональные новости");
/*if($_GET['sort'] == "main" || empty($_GET['sort'])){
	$Sect = "Главные новости";
}else{
	$Sect = "Региональные новости";
}*/?>
<?
$Cate = "";
if ($_GET['cat'] == "d") {
	$Cate = "Добровольчество";
} else {
	if ($_GET['cat'] == "p") {
		$Cate = "Патриотика";
	}
}?>



<?
$Tag = "";
if ($_GET['tag'] == "a") {
	$Tag = "Актуально";
} else {
	if ($_GET['tag'] == "y") {
		$Tag = "Дайджест";
	}
};
if ($_GET['tag'] == "y") {
	$Tag = "Дайджест";
} else {
	if ($_GET['tag'] == "v") {
		$Tag = "Добро в России";
	}
};
if ($_GET['tag'] == "v") {
	$Tag = "Добро в России";
} else {
	if ($_GET['tag'] == "i") {
		$Tag = "История недели";
	}
};
if ($_GET['tag'] == "i") {
	$Tag = "История недели";
} else {
	if ($_GET['tag'] == "l") {
		$Tag = "Люди говорят";
	}
};
if ($_GET['tag'] == "l") {
	$Tag = "Люди говорят";
} else {
	if ($_GET['tag'] == "o") {
		$Tag = "Официально";
	}
};
if ($_GET['tag'] == "o") {
	$Tag = "ОФициально";
} else {
	if ($_GET['tag'] == "t") {
		$Tag = "Патриотика";
	}
};
if ($_GET['tag'] == "t") {
	$Tag = "Патриотика";
} else {
	if ($_GET['tag'] == "u") {
		$Tag = "Принимай участие";
	}
};
if ($_GET['tag'] == "u") {
	$Tag = "Принимай участие";
} else {
	if ($_GET['tag'] == "s") {
		$Tag = "События недели";
	}
};
if ($_GET['tag'] == "s") {
	$Tag = "События недели";
} else {
	if ($_GET['tag'] == "a") {
		$Tag = "Актуально";
	}
};
?>

<?
if(CModule::IncludeModule("iblock")){
	$id = array();
	$arSelect = Array("ID", "DATE_ACTIVE_FROM");
	$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "PROPERTY_SECT_VALUE"=>$Sect, "PROPERTY_CATEGORY_VALUE"=>$Cate, "PROPERTY_TAG_VALUE"=>$Tag);
	$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC", "ID" => "ASC"), $arFilter, false, Array("nPageSize"=>9), $arSelect);
	while($arFields = $res->GetNext()){
		$id[] = $arFields["ID"];
	}
}
?>
<?//$GLOBALS["arrFilter"] = array("PROPERTY_TAG_VALUE"=>$Tag);?>


<div class="news">
	<div class="header"></div>
	<div class="container">
		<div class="mediateca__header">
			<div class="page-title"><?= $APPLICATION->getTitle(); ?></div>
            <div class="mediateca__controller">
            <a href="<? echo $APPLICATION->GetCurPageParam("", array("cat")); ?>"
               class="mediateca__link<? if ($_GET['cat'] == "" || empty($_GET['cat'])) {
                   echo " active";
               } ?>">Все</a>
            <a href="<? echo $APPLICATION->GetCurPageParam("cat=d", array("cat")); ?>"
               class="mediateca__link<? if ($_GET['cat'] == "d") {
                   echo " active";
               } ?>">Добровольчество</a>
            <a href="<? echo $APPLICATION->GetCurPageParam("cat=p", array("cat")); ?>"
               class="mediateca__link<? if ($_GET['cat'] == "p") {
                   echo " active";
               } ?>">Патриотика</a>
        </div>
        </div>
        <div class="doc-page__search w-100">
            <div class="doc-page__select w-100">
                <span class="doc-page__select-label">Рубрика</span>
                <?/*
				<select class="js-type-document select-100" width="100%" style="display: none;">
                    <option>Все</option>
                    <option>Актуально</option>
                    <option>Дайджест</option>
                    <option>Добро в России</option>
                    <option>История недели</option>
                    <option>Люди говорят</option>
                    <option>Официально</option>
                    <option>Патриотика</option>
                    <option>Принимай участие</option>
                    <option>События недели</option>
                </select>
				*/?>
                <div class="nice-select select-100" tabindex="0">
				<?
				if ($_GET['tag'] == "a") {?>
					<span class="current">Актуально</span>
				<?} else if ($_GET['tag'] == "y") {?>
					<span class="current">Дайджест</span>
				<?} else if ($_GET['tag'] == "v") {?>
					<span class="current">Добро в России</span>
				<?} else if ($_GET['tag'] == "i") {?>
					<span class="current">История недели</span>
				<?} else if ($_GET['tag'] == "l") {?>
					<span class="current">Люди говорят</span>
				<?} else if ($_GET['tag'] == "o") {?>
					<span class="current">ОФициально</span>
				<?} else if ($_GET['tag'] == "t") {?>
					<span class="current">Патриотика</span>
				<?} else if ($_GET['tag'] == "u") {?>
					<span class="current">Принимай участие</span>
				<?} else if ($_GET['tag'] == "s") {?>
					<span class="current">События недели</span>
				<?} else {?>
					<span class="current">Все</span>
				<?};
				?>
                    <ul class="list">
                        <li class="option<? if ($_GET['tag'] == "" || empty($_GET['tag'])) {
					        echo " selected";
				            } ?>" data-value="Все">
                        <a href="<? echo $APPLICATION->GetCurPageParam("", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "" || empty($_GET['tag'])) {
					        echo " active";
				            } ?>">Все</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "a") {
					        echo " selected";
				            } ?>" data-value="Актуально">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=a", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "a") {
					        echo " active";
				            } ?>">Актуально</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "y") {
					        echo " selected";
				            } ?>" data-value="Дайджест">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=y", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "y") {
					        echo " active";
				            } ?>">Дайджест</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "v") {
					        echo " selected";
				            } ?>" data-value="Добро в России">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=v", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "v") {
					        echo " active";
				            } ?>">Добро в России</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "i") {
					        echo " selected";
				            } ?>" data-value="История недели">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=i", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "i") {
					        echo " active";
				            } ?>">История недели</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "l") {
					        echo " selected";
				            } ?>" data-value="Люди говорят">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=l", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "l") {
					        echo " active";
				            } ?>">Люди говорят</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "o") {
					        echo " selected";
				            } ?>" data-value="Официально">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=o", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "o") {
					        echo " active";
				            } ?>">Официально</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "t") {
					        echo " selected";
				            } ?>" data-value="Патриотика">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=t", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "t") {
					        echo " active";
				            } ?>">Патриотика</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "u") {
					        echo " selected";
				            } ?>" data-value="Принимай участие">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=u", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "u") {
					        echo " active";
				            } ?>">Принимай участие</a>
                        </li>
                        <li class="option<? if ($_GET['tag'] == "s") {
					        echo " selected";
				            } ?>" data-value="События недели">
                        <a href="<? echo $APPLICATION->GetCurPageParam("tag=s", array("tag")); ?>"
				            class="mediateca__link<? if ($_GET['tag'] == "s") {
					        echo " active";
				            } ?>">События недели</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?/* if ($arParams["USE_FILTER"] == "Y"):
        $term = '';
        if (isset($_GET['term']) && !empty($_GET['term'])) {
            $term = trim($_GET['term']);
        }
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.filter",
            "",
            Array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "AJAX_MODE" => "Y",
                "INSTANT_RELOAD" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_HISTORY" => "Y",
                "SEARCH_TERM" => $term,
            ),
            $component
        );
        ?>
        <? endif ?>
        <? if ($arParams["USE_SEARCH"] == "Y"): ?>
            <?= GetMessage("SEARCH_LABEL") ?><? $APPLICATION->IncludeComponent(
                "bitrix:search.form",
                "flat",
                Array(
                    "PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"]
                ),
                $component
            ); ?>
        <? endif */?>
    </div>
    
    <?
/*$arrFilter = [];

if ($_GET['cat'] == "d") {
    $arrFilter = [
        'PROPERTY_CATEGORY_VALUE' => "Добровольчество",
    ];
} elseif ($_GET['cat'] == "p") {
    $arrFilter = [
        'PROPERTY_CATEGORY_VALUE' => "Патриотика",
    ];
} */?>

<? //$GLOBALS[$arParams['FILTER_NAME']] = $arrFilter;?>


<?$GLOBALS["arrFilter"] = array("!ID"=>$id, "PROPERTY_SECT_VALUE"=>$Sect, "PROPERTY_CATEGORY_VALUE"=>$Cate, "PROPERTY_TAG_VALUE"=>$Tag);?>

<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "PARTNER_IBLOCK_ID" => $arParams["PARTNER_IBLOCK_ID"],
        "NEWS_COUNT" => $arParams["NEWS_COUNT"],

        "SORT_BY1" => $arParams["SORT_BY1"],
        "SORT_ORDER1" => $arParams["SORT_ORDER1"],
        "SORT_BY2" => $arParams["SORT_BY2"],
        "SORT_ORDER2" => $arParams["SORT_ORDER2"],

        "USE_FILTER" => "Y",
        "FILTER_NAME" => "arrFilter",
        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "SEARCH_PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"],

        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],

        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],

        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",

        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
        "SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],

        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

        "USE_RATING" => $arParams["USE_RATING"],
        "DISPLAY_AS_RATING" => $arParams["DISPLAY_AS_RATING"],
        "MAX_VOTE" => $arParams["MAX_VOTE"],
        "VOTE_NAMES" => $arParams["VOTE_NAMES"],

        "USE_SHARE" => $arParams["LIST_USE_SHARE"],
        "SHARE_HIDE" => $arParams["SHARE_HIDE"],
        "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
        "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
        "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
        "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],

        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
    ),
    $component
); ?>


</div>
