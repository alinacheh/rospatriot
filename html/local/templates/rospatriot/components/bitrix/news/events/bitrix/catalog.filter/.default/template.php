<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$dt = new DateTime();
if (isset($_GET['events_from_date'])) {
    $dt = new DateTime($_GET['events_from_date']);
}
$dtPrev = (clone $dt)->modify('-1 month');
$dtNext = (clone $dt)->modify('+1 month');
?>
<form name="<?= $arResult["FILTER_NAME"] . "_form" ?>" action="<?= $arResult["FORM_ACTION"] ?>"
      method="get">
    <div class="events__top">
        <a href="#" data-from="<?= $dtPrev->format('Y-m') . '-01'; ?>"
           data-to="<?= $dtPrev->format('Y-m-t'); ?>" class="events__link">
            <svg class="icon__rg" width="24px" height="26.3px">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
            </svg>
        </a>
        <div class="events__tit"><?= CIBlockFormatProperties::DateFormat("f", $dt->getTimestamp()) ?>, <?=
            CIBlockFormatProperties::DateFormat("Y", $dt->getTimestamp()) ?></div>
        <a href="#" data-from="<?= $dtNext->format('Y-m') . '-01'; ?>"
           data-to="<?= $dtNext->format('Y-m-t'); ?>" class="events__link">
            <svg class="icon__rg" width="24px" height="26.3px">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
            </svg>
        </a>
    </div>
    <? foreach ($arResult["ITEMS"] as $arItem):
        if (array_key_exists("HIDDEN", $arItem)):
            echo $arItem["INPUT"];
        endif;
    endforeach; ?>
    <input type="hidden" class="date-from" name="events_from_date"
           value="<?= $dt->format('Y-m') . '-01'; ?>">
    <input type="hidden" class="date-to" name="events_to_date"
           value="<?= $dt->format('Y-m-t'); ?>">

    <input type="submit" class="btn-submit" style="display:none;" name="set_filter"
           value="<?= GetMessage("IBLOCK_SET_FILTER") ?>"/>
    <input type="hidden" name="set_filter" value="Y"/>
    <input type="submit" style="display:none;" name="del_filter" value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>"/>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('.events__top .events__link').on('click', function (e) {
            e.preventDefault();
            var fromDt = $(this).data('from'),
                toDt = $(this).data('to'),
                $form = $(this).closest('form');
            $form.find('.date-from').val(fromDt);
            $form.find('.date-to').val(toDt);
            $form.find('.btn-submit').trigger('click');
        });
    });
</script>
