<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arParams["USE_RSS"] == "Y"): ?>
    <?
    if (method_exists($APPLICATION, 'addheadstring')) {
        $APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" href="' . $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] . '" />');
    }
    ?>
    <a href="<?= $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["rss"] ?>" title="rss" target="_self"><img alt="RSS"
                                                                                                             src="<?= $templateFolder ?>/images/gif-light/feed-icon-16x16.gif"
                                                                                                             border="0"
                                                                                                             align="right"/></a>
<? endif ?>

<div class="container">
    <div class="mediateca__header">
        <div class="page-title"><?= $APPLICATION->getTitle(); ?></div>
        <div class="mediateca__controller">
            <a href="<? echo $APPLICATION->GetCurPageParam("", array("sort")); ?>"
               class="mediateca__link<? if ($_GET['sort'] == "" || empty($_GET['sort'])) {
                   echo " active";
               } ?>">Все</a>
            <a href="<? echo $APPLICATION->GetCurPageParam("sort=d", array("sort")); ?>"
               class="mediateca__link<? if ($_GET['sort'] == "d") {
                   echo " active";
               } ?>">Добровольчество</a>
            <a href="<? echo $APPLICATION->GetCurPageParam("sort=p", array("sort")); ?>"
               class="mediateca__link<? if ($_GET['sort'] == "p") {
                   echo " active";
               } ?>">Патриотика</a>
        </div>
        <? /*<div class="mediateca__controller">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "events", Array(
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"MAX_LEVEL" => "1",	// Уровень вложенности меню
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_TYPE" => "A",	// Тип кеширования
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"ROOT_MENU_TYPE" => "events",	// Тип меню для первого уровня
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"COMPONENT_TEMPLATE" => ".default"
				),
				false
			);?>
			</div>*/ ?>
    </div>
    <? if ($arParams["USE_FILTER"] == "Y"): ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.filter",
            "",
            Array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
            ),
            $component
        );
        ?>
    <? endif ?>
    <? if ($arParams["USE_SEARCH"] == "Y"): ?>
        <?= GetMessage("SEARCH_LABEL") ?><? $APPLICATION->IncludeComponent(
            "bitrix:search.form",
            "flat",
            Array(
                "PAGE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["search"]
            ),
            $component
        ); ?>
    <? endif ?>
</div>

<?
$arrFilter = [];

if ($_GET['sort'] == "d") {
    $arrFilter = [
        'PROPERTY_CATEGORY_VALUE' => "Добровольчество",
    ];
} elseif ($_GET['sort'] == "p") {
    $arrFilter = [
        'PROPERTY_CATEGORY_VALUE' => "Патриотика",
    ];
}
if (isset($_GET['events_from_date']) &&
    preg_match('#^([\d]{4})-([\d]{2})-([\d]{2})$#', $_GET['events_from_date'])
) {
    $rangeStartDate = $_GET['events_from_date'];
} else {
    $rangeStartDate = (new DateTime())->format('Y-m') . '-01';
}
if (isset($_GET['events_to_date']) &&
    preg_match('#^([\d]{4})-([\d]{2})-([\d]{2})$#', $_GET['events_to_date'])
) {
    $rangeEndDate = $_GET['events_to_date'];
} else {
    $rangeEndDate = (new DateTime())->format('Y-m-t');
}

$cacheId = 'events_filter_' . $rangeStartDate . '_' . $rangeEndDate . ($_GET['sort'] ?? '');
$cache = new CPHPCache();
if ($cache->InitCache(600, $cacheId, false)) {
    $arrFilter = $cache->GetVars();
} else {
    $cache->StartDataCache(600, $cacheId, false);

    $rangeStartDate = strtotime($rangeStartDate);
    $rangeEndDate = strtotime($rangeEndDate);

    $propertyRes = $DB->Query('SELECT ' .
        'DISTINCT(`v1`.`IBLOCK_ELEMENT_ID`) as `ID` ' .
        'FROM ( ' .
        'SELECT `EP`.VALUE, `EP`.`IBLOCK_ELEMENT_ID` FROM `b_iblock_element_property` as `EP` ' .
        'INNER JOIN `b_iblock_property` as `P` ON(`EP`.`IBLOCK_PROPERTY_ID`=`P`.`ID` AND ' .
        '`P`.`IBLOCK_ID` = \'' . $arParams['IBLOCK_ID'] . '\' AND `P`.`CODE`=\'ATT_START\') ' .
        ') as `v1` ' .
        'INNER JOIN ' .
        '(' .
        'SELECT `EP`.VALUE, `EP`.`IBLOCK_ELEMENT_ID` FROM `b_iblock_element_property` as `EP` ' .
        'INNER JOIN `b_iblock_property` as `P` ON(`EP`.`IBLOCK_PROPERTY_ID`=`P`.`ID` AND ' .
        '`P`.`IBLOCK_ID` = \'' . $arParams['IBLOCK_ID'] . '\' AND `P`.`CODE`=\'ATT_END\') ' .
        ') as `v2` ON(`v1`.`IBLOCK_ELEMENT_ID`=`v2`.`IBLOCK_ELEMENT_ID`) ' .
        'WHERE (' .
        '(UNIX_TIMESTAMP(`v1`.`VALUE`) <= ' . $rangeEndDate . ' AND UNIX_TIMESTAMP(`v1`.`VALUE`) >= ' . $rangeStartDate . ') OR ' .
        '(UNIX_TIMESTAMP(`v2`.`VALUE`) <= ' . $rangeEndDate . ' AND UNIX_TIMESTAMP(`v2`.`VALUE`) >= ' . $rangeStartDate . ') OR ' .
        '(UNIX_TIMESTAMP(`v1`.`VALUE`) <= ' . $rangeStartDate . ' AND ' .
        'UNIX_TIMESTAMP(`v2`.`VALUE`) >= ' . $rangeEndDate . ')' .
        ')'
    );

    $IDs = [];
    while ($row = $propertyRes->Fetch()) {
        array_push($IDs, $row['ID']);
    }
    if (!empty($IDs)) {
        $arrFilter['ID'] = $IDs;
        $cache->EndDataCache($arrFilter);
    } else {
        $arrFilter['ID'] = '-1';
        $cache->abortDataCache();
    }
} ?>

<? $GLOBALS[$arParams['FILTER_NAME']] = $arrFilter;?>
<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "NEWS_COUNT" => $arParams["NEWS_COUNT"],
        "SORT_BY1" => $arParams["SORT_BY1"],
        "SORT_ORDER1" => $arParams["SORT_ORDER1"],
        "SORT_BY2" => $arParams["SORT_BY2"],
        "SORT_ORDER2" => $arParams["SORT_ORDER2"],
        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "USE_FILTER" => "Y",
        "FILTER_NAME" => $arParams['FILTER_NAME'],
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
    ),
    $component
); ?>
