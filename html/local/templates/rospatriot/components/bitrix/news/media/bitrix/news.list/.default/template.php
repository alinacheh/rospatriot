<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<div class="container-full">
	<div class="mediateca__flex news-list">
	<?$i = 0;?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="mediateca-item news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="javascript:;" data-fancybox="hello<?=$i;?>" data-src="<?if($arItem["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"] == "Видео"){echo $arItem["DISPLAY_PROPERTIES"]["ATT_VIDEO"]["VALUE"];}else{echo $arItem["PREVIEW_PICTURE"]["SRC"];}?>" class="mediateca-item__wrapper">
				<div class="mediateca-item__background" style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
				</div>
				<div class="mediateca-item__content">
					<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
						<div class="mediateca-item__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
					<?endif?>
					<div class="mediateca-item__play">
						<?if($arItem["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"] == "Видео"):?>
							<div class="mediateca-item__icon">
								<svg class="icon__play" width="14px" height="17px">
									<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#play"></use>
								</svg>
							</div>
							<div class="mediateca-item__txt">смотреть видео</div>
						<?elseif($arItem["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"] == "Фотогалерея"):?>
							<div class="mediateca-item__icon">
								<svg class="icon__landscape-fill" width="24px" height="24px">
									<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#landscape-fill"></use>
								</svg>
							</div>
							<?$count = 1;?>
							<?foreach($arItem["DISPLAY_PROPERTIES"]["ATT_PHOTO"]["FILE_VALUE"] as $PHOTO){
								$count++;
							}?>
							<div class="mediateca-item__txt"><?=$count?> фото</div>
						<?endif;?>
					</div>
					<div class="mediateca-item__text"><?echo $arItem["NAME"];?></div>
				</div>
			</a>
			<?foreach($arItem["DISPLAY_PROPERTIES"]["ATT_PHOTO"]["FILE_VALUE"] as $PHOTO):?>
				<a href="javascript:;" data-fancybox="hello<?=$i;?>" data-src="<?=$PHOTO["SRC"]?>" style="display:none;"></a>
			<?endforeach;?>
			<?$i++;?>
		</div>
	<?endforeach;?>
	</div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>