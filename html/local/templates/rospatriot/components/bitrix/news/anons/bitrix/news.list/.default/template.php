<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
	<? foreach ($arResult["ITEMS"] as $arItem):
            $eventStartDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"], CSite::GetDateFormat()) :
                MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat());
            $eventEndDateTimestamp = isset($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"]) ?
                MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()) :
                $eventStartDateTimestamp;
            ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
		<div class="announcement__item">
			<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>" class="announcement__link"><? echo $arItem["NAME"] ?></a>
			<? if (empty($eventEndDateTimestamp)): ?>
				<div class="announcement__date">
					<?= CIBlockFormatProperties::DateFormat("j F Y", $eventStartDateTimestamp); ?>
				</div>
			<? else: ?>
				<? if (CIBlockFormatProperties::DateFormat("f", $eventStartDateTimestamp) === CIBlockFormatProperties::DateFormat("f", $eventEndDateTimestamp)): ?>
					<div class="announcement__date">
						<?= CIBlockFormatProperties::DateFormat("j", $eventStartDateTimestamp); ?><?=$eventStartDateTimestamp != $eventEndDateTimestamp ? '-' . CIBlockFormatProperties::DateFormat("j", $eventEndDateTimestamp) : ''; ?> <?= CIBlockFormatProperties::DateFormat("F Y", $eventStartDateTimestamp); ?>
					</div>
				<? else: ?>
					<div class="announcement__date">
						<?= CIBlockFormatProperties::DateFormat("j F", $eventStartDateTimestamp); ?> - <? echo CIBlockFormatProperties::DateFormat("j F", $eventEndDateTimestamp); ?> <?= CIBlockFormatProperties::DateFormat("Y", $eventStartDateTimestamp); ?>
					</div>
				<? endif; ?>
			<? endif; ?>
		</div>
	<? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
