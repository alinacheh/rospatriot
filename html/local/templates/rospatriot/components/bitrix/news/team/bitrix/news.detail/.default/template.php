<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- begin::news -->
<div class="news-inner">
    <div class="container">
        <div class="news-inner__content">
            <?php if ($arParams["USE_SHARE"] == "Y"): ?>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.share",
                    'news',
                    array(
                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                        "PAGE_TITLE" => $arResult["~NAME"],
                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                        "HIDE" => $arParams["SHARE_HIDE"],
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
                ?>
            <?php endif ?>
			<?$eventStartDateTimestamp = isset($arResult["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"]) ?
                MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["ATT_START"]["VALUE"], CSite::GetDateFormat()) :
                MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat());
            $eventEndDateTimestamp = isset($arResult["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"]) ?
                MakeTimeStamp($arResult["DISPLAY_PROPERTIES"]["ATT_END"]["VALUE"], CSite::GetDateFormat()) :
                $eventStartDateTimestamp;?>
            <div class="html">
                <h1><?= $arResult['NAME'] ?></h1>
                <? if (empty($eventEndDateTimestamp)): ?>
					<div class="news-inner__date">
						<?= CIBlockFormatProperties::DateFormat("j F Y", $eventStartDateTimestamp); ?>
					</div>
				<? else: ?>
					<? if (CIBlockFormatProperties::DateFormat("f", $eventStartDateTimestamp) === CIBlockFormatProperties::DateFormat("f", $eventEndDateTimestamp)): ?>
						<div class="news-inner__date">
							<?= CIBlockFormatProperties::DateFormat("j", $eventStartDateTimestamp); ?><?=$eventStartDateTimestamp != $eventEndDateTimestamp ? '-' . CIBlockFormatProperties::DateFormat("j", $eventEndDateTimestamp) : ''; ?> <?= CIBlockFormatProperties::DateFormat("F Y", $eventStartDateTimestamp); ?>
						</div>
					<? else: ?>
						<div class="news-inner__date">
							<?= CIBlockFormatProperties::DateFormat("j F", $eventStartDateTimestamp); ?> - <? echo CIBlockFormatProperties::DateFormat("j F", $eventEndDateTimestamp); ?> <?= CIBlockFormatProperties::DateFormat("Y", $eventStartDateTimestamp); ?>
						</div>
					<? endif; ?>
				<? endif; ?>
                <p class="news-inner__desc"><?= $arResult['PREVIEW_TEXT'] ?></p>
                <?php if ($arResult['SLIDER']): ?>
                    <div class="news-inner__slider">
                        <div class="news-inner__navigation">
                            <div class="news-inner__arrow prev">
                                <svg class="icon__rg" width="24px" height="26.3px">
                                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
                                </svg>
                            </div>
                            <div class="news-inner__arrow next">
                                <svg class="icon__rg" width="24px" height="26.3px">
                                    <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="news-inner__top">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php foreach ($arResult['SLIDER'] as $src): ?>
                                        <div class="swiper-slide" href="<?= $src['SRC']; ?>" data-fancybox="gallery">
                                            <div class="news-inner__sl"
                                                 style="background-image: url('<?= $src['SRC']; ?>')">
                                                <img src="<?= $src['SRC']; ?>" alt="">
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="news-inner__thumbs">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php foreach ($arResult['SLIDER'] as $src): ?>
                                        <div class="swiper-slide" href="<?= $src['SRC']; ?>">
                                            <div class="news-inner__sl"
                                                 style="background-image: url('<?= $src['SRC']; ?>')">
                                                <img src="<?= $src['SRC']; ?>" alt="">
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="details">
                    <?= $arResult['DETAIL_TEXT']; ?>
                </div>
				<div class="announcement__footer">
                    <?= $arResult['PROPERTIES']["REFERENCE"]["~VALUE"]["TEXT"]; ?>
					<p>Официальный сайт <?= $arResult['PROPERTIES']["ATT_NAME"]["VALUE"]; ?>: 
						<a href="http://<?= $arResult['PROPERTIES']["ATT_URL"]["VALUE"]; ?>"><?= $arResult['PROPERTIES']["ATT_URL"]["VALUE"]; ?></a>
					</p>
					<p>Контакты для СМИ: <?= $arResult['PROPERTIES']["ATT_FORSMI"]["~VALUE"]["TEXT"]; ?></p>
                </div>
            </div>
        </div>
        <div class="news-inner__footer">
            <a href="<?= $arResult['LIST_PAGE_URL']; ?>" class="news-inner__back">
                <div class="news-inner__circle">
                    <svg class="icon__rg" width="24px" height="26.3px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#rg"></use>
                    </svg>
                </div>
                вернуться назад
            </a>
            <noindex>
				<div class="news-inner__right">
					<div class="news-inner__gray">Поделись новостью
						<br>с друзьями
					</div>
					<div class="news-inner__socials">
						<a href="http://vk.com/share.php?url=http://rospatriot-bitrix.dev-vps.ru<?= $arResult['DETAIL_PAGE_URL']; ?>&title=<?= $arResult['NAME'] ?>&description=<?echo strip_tags($arResult['PREVIEW_TEXT']);?>&image=http://rospatriot-bitrix.dev-vps.ru<?= $arResult['PREVIEW_PICTURE']['SRC'] ?>&noparse=true" target="_blank" class="event-modal-link vk" onclick="window.open(this.href,this.target,'width=300,height=300');return false;">
							<svg class="icon__vk-logo" width="548px" height="548px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#vk-logo"></use>
							</svg>
						</a>
						<a href="https://www.facebook.com/sharer.php?src=sp&amp;u=http://rospatriot-bitrix.dev-vps.ru<?=$arResult["DETAIL_PAGE_URL"]?>&amp;title=<?echo $arResult["NAME"]?>&amp;description=<?echo strip_tags($arResult["PREVIEW_TEXT"]);?>&amp;picture=http://rospatriot-bitrix.dev-vps.ru<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>&amp;utm_source=share2" target="_blank" class="event-modal-link fa" onclick="window.open(this.href,this.target,'width=300,height=300');return false;">
							<svg class="icon__facebook-logo" width="96px" height="96px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#facebook-logo"></use>
							</svg>
						</a>
						<a href="https://twitter.com/share?url=http://rospatriot-bitrix.dev-vps.ru<?=$arResult["DETAIL_PAGE_URL"]?>&text=<?echo $arResult["NAME"]?>" target="_blank" class="event-modal-link od" onclick="window.open(this.href,this.target,'width=300,height=300');return false;">
							<svg class="icon__twitter-logo" width="612px" height="612px">
								<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#twitter-logo"></use>
							</svg>
						</a>
					</div>
				</div>
			</noindex>
        </div>
    </div>
</div>
<!-- end::news -->
