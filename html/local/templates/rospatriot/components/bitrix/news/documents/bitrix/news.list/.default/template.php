<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="doc__item news-item">
		<div class="doc-flex doc-sb">
			<div>
				<div class="doc__name"><?echo $arItem["NAME"]?></div>
				<div class="doc-flex">
					<div class="doc__type"><?=$arItem["DISPLAY_PROPERTIES"]["ATT_TYPE"]["VALUE"]?></div>
					<div class="doc__date">Добавлен: <?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
				</div>
			</div>
			<div class="doc__file">
				<a download="" href="<?=$arItem["DISPLAY_PROPERTIES"]["ATT_FILE"]["FILE_VALUE"]["SRC"]?>" class="doc__download">
					<span class="icon">
						<svg class="icon__file-download-r" width="24px" height="24px">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#file-download-r"></use>
						</svg>
					</span>
				</a>
				<?
				$size = $arItem["DISPLAY_PROPERTIES"]["ATT_FILE"]["FILE_VALUE"]["FILE_SIZE"];
				$sizes = array('Б', 'КБ', 'МБ', 'ГБ', 'ТБ', 'Pb', 'Eb', 'Zb', 'Yb');
				for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024;
				if($i > 1):
					$round = 2;
				else:
					$round = 0;
				endif;
				$size = round($size,$round)." ".$sizes[$i];
				$exten = $arItem["DISPLAY_PROPERTIES"]["ATT_FILE"]["FILE_VALUE"]["FILE_NAME"];
				$extension = mb_substr($exten, mb_strrpos($exten, '.')+1);
				?>
				<div class="doc__size"><?echo mb_strtoupper($extension);?>, <?echo $size;?></div>
			</div>
		</div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
