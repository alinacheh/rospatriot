<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="container">
	<div class="doc-page__header">
		<div class="title-page">Документы</div>
		<div class="doc-page__controller">
			<a href="<?echo $APPLICATION->GetCurPageParam("sort=data", array("sort"));?>" class="doc-page__link<?if($_GET['sort'] != "name"){echo " active";}?>">По дате</a>
			<a href="<?echo $APPLICATION->GetCurPageParam("sort=name", array("sort"));?>" class="doc-page__link<?if($_GET['sort'] == "name"){echo " active";}?>">По названию</a>
		</div>
	</div>
	<div class="doc-page__search">
		<div class="group-search">
			<form action="" method="get">
			<?if($arParams["USE_SUGGEST"] === "Y"):
				if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
				{
					$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
					$obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
					$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
				}
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:search.suggest.input",
					"",
					array(
						"NAME" => "q",
						"VALUE" => $arResult["REQUEST"]["~QUERY"],
						"INPUT_SIZE" => 40,
						"DROPDOWN_SIZE" => 10,
						"FILTER_MD5" => $arResult["FILTER_MD5"],
					),
					$component, array("HIDE_ICONS" => "Y")
				);?>
			<?else:?>
				<input type="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" />
			<?endif;?>
			<?if($arParams["SHOW_WHERE"]):?>
				&nbsp;<select name="where">
				<option value=""><?=GetMessage("SEARCH_ALL")?></option>
				<?foreach($arResult["DROPDOWN"] as $key=>$value):?>
				<option value="<?=$key?>"<?if($arResult["REQUEST"]["WHERE"]==$key) echo " selected"?>><?=$value?></option>
				<?endforeach?>
				</select>
			<?endif;?>
				<div class="group-search__close">
					<svg class="icon__search-2-line">
						<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close-line"></use>
					</svg>
				</div>
				<button class="group-search__btn" type="submit" value="<?=GetMessage("SEARCH_GO")?>">
					<span>
						<svg class="icon__search-2-line" width="24px" height="24px">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
						</svg>
					</span>
				</button>
				<?/*<label for="search">
					<span>
						<svg class="icon__search-2-line" width="24px" height="24px">
							<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
						</svg>
					</span>
				</label>
				<input id="search" style="display: none;" type="submit" value="<?=GetMessage("SEARCH_GO")?>" />
				*/?>
				<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
			<?if($arParams["SHOW_WHEN"]):?>
				<script>
				var switch_search_params = function()
				{
					var sp = document.getElementById('search_params');
					var flag;
					var i;
			
					if(sp.style.display == 'none')
					{
						flag = false;
						sp.style.display = 'block'
					}
					else
					{
						flag = true;
						sp.style.display = 'none';
					}
			
					var from = document.getElementsByName('from');
					for(i = 0; i < from.length; i++)
						if(from[i].type.toLowerCase() == 'text')
							from[i].disabled = flag;
			
					var to = document.getElementsByName('to');
					for(i = 0; i < to.length; i++)
						if(to[i].type.toLowerCase() == 'text')
							to[i].disabled = flag;
			
					return false;
				}
				</script>
				<br /><a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
				<div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'Y',
							'INPUT_NAME' => 'from',
							'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
							'INPUT_NAME_FINISH' => 'to',
							'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
							'INPUT_ADDITIONAL_ATTR' => 'size="10"',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
				</div>
			<?endif?>
			</form>
		</div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.filter",
			"documents",
			Array(
				"IBLOCK_TYPE" => "media",
				"IBLOCK_ID" => "7",
				"FILTER_NAME" => "searchFilter",
				"FIELD_CODE" => array(),
				"PROPERTY_CODE" => array("ATT_TYPE"),
				"PRICE_CODE" => array(),
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"LIST_HEIGHT" => "5",
				"TEXT_WIDTH" => "20",
				"NUMBER_WIDTH" => "5",
				"SAVE_IN_SESSION" => "N",
				"PAGER_PARAMS_NAME" => "arrPager"
			),
			$component
		);?> 
	</div>
</div>

<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess container">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>

<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
<?elseif($arResult["ERROR_CODE"]!=0):?>
	<p><?=GetMessage("SEARCH_ERROR")?></p>
	<?ShowError($arResult["ERROR_TEXT"]);?>
	<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
	<br /><br />
	<p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
	<table border="0" cellpadding="5">
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
			<td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
			<td><?=GetMessage("SEARCH_AND_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
			<td><?=GetMessage("SEARCH_OR_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
			<td><?=GetMessage("SEARCH_NOT_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top">( )</td>
			<td valign="top">&nbsp;</td>
			<td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
		</tr>
	</table>
<?elseif(count($arResult["SEARCH"])>0):?>
	<?$searchId = array();?>
	<?foreach($arResult["SEARCH"] as $arItem):?>
		<?$searchId[] = $arItem["ITEM_ID"];?>
	<?endforeach;?>
	<?$GLOBALS["searchFilter"][] = array("ID"=>$searchId);?>
	<?if($_GET['sort'] == "name"){
		$sort = "NAME";
		$order = "ASC";
	} else {
		$sort = "ACTIVE_FROM";
		$order = "DESC";
	}?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"documents",
		array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"ADD_SECTIONS_CHAIN" => "N",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"USE_FILTER" => "Y",
							"FILTER_NAME" => "searchFilter",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"IBLOCK_ID" => "7",
							"IBLOCK_TYPE" => "media",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"INCLUDE_SUBSECTIONS" => "Y",
							"MESSAGE_404" => "",
							"NEWS_COUNT" => "20",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Новости",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "",
							"PREVIEW_TRUNCATE_LEN" => "",
							"PROPERTY_CODE" => array(
								0 => "CATEGORY",
								1 => "ATT_TYPE",
								2 => "ATT_FILE",
								3 => "",
							),
							"SET_BROWSER_TITLE" => "N",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "N",
							"SHOW_404" => "N",
							"SORT_BY1" => $sort,
							"SORT_BY2" => "SORT",
							"SORT_ORDER1" => $order,
							"SORT_ORDER2" => "ASC",
							"STRICT_SECTION_CHECK" => "N"
						),
		false
	);?>

<?else:?>
	<div class="container">
		<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
	</div>
<?endif;?>
