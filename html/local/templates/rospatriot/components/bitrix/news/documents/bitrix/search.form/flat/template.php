<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="group-search">
    <form action="<?= $arResult["FORM_ACTION"] ?>">
        <? if ($arParams["USE_SUGGEST"] === "Y"): ?><? $APPLICATION->IncludeComponent(
            "bitrix:search.suggest.input",
            "",
            array(
                "NAME" => "q",
                "VALUE" => "",
                "INPUT_SIZE" => 15,
                "DROPDOWN_SIZE" => 10,
            ),
            $component, array("HIDE_ICONS" => "Y")
        ); ?>
        <? else: ?>
            <input type="text" name="q" value="" placeholder="Поиск документов"/>
        <? endif; ?>
        <div class="group-search__close">
            <svg class="icon__search-2-line">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close-line"></use>
            </svg>
        </div>
		<button class="group-search__btn" name="s" type="submit" value="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>">
			<span>
				<svg class="icon__search-2-line" width="24px" height="24px">
					<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
				</svg>
			</span>
		</button>
        <?/*<label for="search">
		<span>
			<svg class="icon__search-2-line" width="24px" height="24px">
				<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
			</svg>
		</span>
        </label>
        <input id="search" style="display: none;" name="s" type="submit" value="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>"/>
		*/?>
    </form>
</div>
