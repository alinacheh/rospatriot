(function ($) {
    $(document).ready(function(){
        $('select.js-type-document').on('change', function () {
            var form = $(this).closest('form');
            form.find('input[name="set_filter"]').trigger('click');
        });
    });
})(jQuery);
