<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<form class="header__search-form" action="<?=$arResult["FORM_ACTION"]?>">
	<div class="header__search--close"></div>
	<?if($arParams["USE_SUGGEST"] === "Y"):?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:search.suggest.input",
				"",
				array(
					"NAME" => "q",
					"VALUE" => "",
					"INPUT_SIZE" => 15,
					"DROPDOWN_SIZE" => 10,
				),
				$component, array("HIDE_ICONS" => "Y")
			);?>
	<?else:?>
		<input placeholder="Поиск" type="text" name="q" value="" size="15" maxlength="50" />
	<?endif;?>
	<button name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" class="contact__form-input search">
		<svg class="icon__search-2-line" width="24px" height="24px">
			<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
		</svg>
	</button>
</form>
<div class="header__search-icon">
	<svg class="icon__search-2-line" width="24px" height="24px">
		<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#search-2-line"></use>
	</svg>
</div>