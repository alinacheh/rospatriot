<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arResult */
/** @var array $arParams */
?>
<noindex>
    <div class="news-inner__fix">
        <a href="https://vk.com/hellomyrussia" target="_blank" class="event-modal-link vk">
            <svg class="icon__vk-logo" width="548px" height="548px">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#vk-logo"></use>
            </svg>
        </a>
        <a href="https://www.facebook.com/rpatriotcentr/" target="_blank" class="event-modal-link fa">
            <svg class="icon__facebook-logo" width="96px" height="96px">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#facebook-logo"></use>
            </svg>
        </a>
        <a href="https://twitter.com/RPatriotCenter" target="_blank" class="event-modal-link od">
            <svg class="icon__twitter-logo" width="612px" height="612px">
                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#twitter-logo"></use>
            </svg>
        </a>
    </div>
</noindex>
