<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"VK_URL" => Array(
	"NAME" => "Ссылка Вконтакте",
	"TYPE" => "STRING",
	"VALUES" => array()
	),
	"INST_URL" => Array(
	"NAME" => "Ссылка Инстаграм",
	"TYPE" => "STRING",
	"VALUES" => array()
	),
	"ODN_URL" => Array(
	"NAME" => "Ссылка Одноклассники",
	"TYPE" => "STRING",
	"VALUES" => array()
	),
	"YT_URL" => Array(
	"NAME" => "Ссылка YouTube",
	"TYPE" => "STRING",
	"VALUES" => array()
	),
);
?>