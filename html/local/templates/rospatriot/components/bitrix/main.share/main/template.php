<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arResult */
/** @var array $arParams */
?>

<div class="piotica__social">
	<div class="piotica__socialtext">страницы проекта в социальных сетях</div>
	<div class="piotica__socialicons">
		<?if(!empty($arParams["VK_URL"])):?>
			<a href="<?=$arParams["VK_URL"]?>" class="piotica__soc">
				<svg class="icon__vk-logo" width="548px" height="548px">
					<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#vk-logo"></use>
				</svg>
			</a>
		<?endif;?>
		<?if(!empty($arParams["INST_URL"])):?>
			<a href="<?=$arParams["INST_URL"]?>" class="piotica__soc">
				<svg class="icon__instagram-logo" width="169px" height="169px">
					<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#instagram-logo"></use>
				</svg>
			</a>
		<?endif;?>
		<?if(!empty($arParams["ODN_URL"])):?>
			<a href="<?=$arParams["ODN_URL"]?>" class="piotica__soc">
				<svg class="icon__odnoklassniki-logo" width="95px" height="95px">
					<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#odnoklassniki-logo"></use>
				</svg>
			</a>
		<?endif;?>
		<?if(!empty($arParams["YT_URL"])):?>
			<a href="<?=$arParams["YT_URL"]?>" class="piotica__soc">
				<svg class="icon__youtube" width="512px" height="512px">
					<use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#youtube"></use>
				</svg>
			</a>
		<?endif;?>
	</div>
</div>