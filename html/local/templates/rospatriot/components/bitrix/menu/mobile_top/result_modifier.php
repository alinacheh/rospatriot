<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 17.08.19
 * Time: 12:33
 */

/** @var $arResult array */

if ($arResult) {
    $result = $arResult;
    $arResult = ['TREE' => []];

    $tree = [];
    $lastParentIndex = -1;
    foreach ($result as $item) {
        if ($item['DEPTH_LEVEL'] < 2) {
            if ($item['IS_PARENT']) {
                $lastParentIndex = $item['ITEM_INDEX'];
                $item['CHILDREN'] = [];
            }
            $tree[$item['ITEM_INDEX']] = $item;
        } else {
            $tree[$lastParentIndex]['CHILDREN'][] = $item;
        }
    }
    $arResult['TREE'] = $tree;
}
