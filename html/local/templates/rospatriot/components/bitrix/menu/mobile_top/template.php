<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arResult */
?>
<?php if ($arResult['TREE']): ?>
    <ul class="menu">
        <?php foreach ($arResult['TREE'] as $item): ?>
            <li class="menu__item<?= $item['IS_PARENT'] ? ' menu__item-subs' : ''; ?>">
                <a href="<?= $item['IS_PARENT'] ? '#' : $item['LINK']; ?>" class="menu__link"><?= $item['TEXT']; ?></a>
                <?php if (isset($item['CHILDREN'])): ?>
                    <div class="menu__lox"></div>
                    <ul class="menu__sub">
                        <?php foreach ($item['CHILDREN'] as $child): ?>
                            <li class="menu__item">
                                <a href="<?= $child['LINK']; ?>" class="menu__link"><?= $child['TEXT']; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
