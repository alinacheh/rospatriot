<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var $arResult array */
?>

<?php if ($arResult['PARENTS']): ?>
    <?php foreach ($arResult['PARENTS'] as $key => $item):
        $pair = $arResult['ITEMS'][$key] ?? null;
        ?>
        <div class="footer__box">
            <div class="footer__row">
                <div>
                    <div class="footer__title"><?= $item['TEXT'] ?></div>
                </div>
                <?php if (isset($item['CHILDREN'])): ?>
                    <ul>
                        <?php foreach ($item['CHILDREN'] as $child): ?>
                            <li>
                                <a href="<?= $child['LINK'] ?>" class="footer__item"><?= $child['TEXT'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <?php if ($pair): ?>
                <div>
                    <a href="<?= $pair['LINK'] ?>" class="footer__title"><?= $pair['TEXT'] ?></a>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
