<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 17.08.19
 * Time: 12:33
 */

/** @var $arResult array */

if ($arResult) {
    $result = $arResult;
    $arResult = ['PARENTS' => [], 'ITEMS' => []];
    $lastParentIndex = 0;
    foreach ($result as $item) {
        if ($item['DEPTH_LEVEL'] < 2) {
            $lastParentIndex = count($arResult['PARENTS']);
            if ($item['IS_PARENT']) {
                $item['CHILDREN'] = [];
                $arResult['PARENTS'][$lastParentIndex] = $item;
            } else {
                $arResult['ITEMS'][] = $item;
            }
        } else {
            $arResult['PARENTS'][$lastParentIndex]['CHILDREN'][] = $item;
        }
    }
}
