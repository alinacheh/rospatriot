<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var $arResult array */
?>
<?php if ($arResult['TREE']): ?>
    <div class="header__menu">
        <ul class="menu">
            <?php foreach ($arResult['TREE'] as $item): ?>
                <li class="menu__item<?= $item['IS_PARENT'] ? ' menu__item-subs' : ''; ?>">
                    <a href="<?= $item['IS_PARENT'] ? '#' : $item['LINK']; ?>"
                       class="menu__link"><?= $item['TEXT']; ?></a>
                    <?php if (isset($item['CHILDREN'])): ?>
                        <ul class="menu__sub">
                            <?php foreach ($item['CHILDREN'] as $child): ?>
                                <li class="menu__item">
                                    <a href="<?= $child['LINK']; ?>" class="menu__link"><?= $child['TEXT']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
