<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arrPFV = $_REQUEST[$arResult['FILTER_NAME'] . "_pf"] ?? (array)$_SESSION[$arResult['FILTER_NAME'] . "arrPFV"];
?>
<form name="<?= $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>"
      method="get">
    
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <? if ($arItem['TYPE'] === 'SELECT'): ?>
                <div class="doc-page__select">
                    <span class="doc-page__select-label"><?= $arItem["NAME"] ?></span>
                    <select class="js-type-document" name="<?= $arItem['INPUT_NAME']; ?>"
                            title="<?= $arItem["NAME"] ?>">
                        <?php foreach ($arItem['LIST'] as $value => $option): ?>
                            <option value="<?= $value; ?>"<?=
                            $arrPFV && $arrPFV['ATT_TYPE'] == $value ? ' selected' : '' ?>><?= $option; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            <? endif ?>
        <? endforeach; ?>

    <? foreach ($arResult["ITEMS"] as $arItem):
        if (array_key_exists("HIDDEN", $arItem)):
            echo $arItem["INPUT"];
        endif;
    endforeach; ?>
    <input type="submit" id="buttonID" style="display:none;" name="set_filter"
           value="<?= GetMessage("IBLOCK_SET_FILTER") ?>"/>
    <input type="hidden" name="set_filter" value="Y"/>
    <input type="submit" style="display:none;" name="del_filter" value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>"/>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        var next = $('.js-type-document').next('.nice-select');
        if (next.length === 0) {
            $('.js-type-document').niceSelect();
            $('select.js-type-document').on('change', function () {
                var form = $(this).closest('form');
                form.find('input[name="set_filter"]').trigger('click');
            });
        }
    });
</script>
