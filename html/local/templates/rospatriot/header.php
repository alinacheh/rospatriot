<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Page\Asset;

IncludeTemplateLangFile(__FILE__);

// template styles
Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css');
Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css');
Asset::getInstance()->addCss('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i,900,900i&display=swap&subset=cyrillic');
Asset::getInstance()->addCss('/local/templates/rospatriot/static/css/main.min.css');
Asset::getInstance()->addCss('/local/templates/rospatriot/static/css/update.css');


Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js');
Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js');
Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js');
Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js');
Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js');
Asset::getInstance()->addJs('/local/templates/rospatriot/static/js/main.js');
Asset::getInstance()->addJs('/local/templates/rospatriot/static/js/site.js');

?>
<!doctype html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">

    <link rel="icon" type="image/x-icon" href="/local/templates/rospatriot/common/favicon.ico">
    <? $APPLICATION->ShowHead(); ?>
</head>

<body class="body">
<?php if($USER->IsAuthorized()):?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<?php endif;?>
<div class="document">
    <div class="document__content">
        <!-- begin::header -->

        <div class="mobile">
            <div class="mobile__header">
                <div class="mobile__title">Меню</div>
                <div class="mobile__close"></div>
            </div>
            <div class="mobile__content">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "mobile_top",
                    [
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => [],
                        "MAX_LEVEL" => 2,
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ],
                    false
                ); ?>
            </div>
            <div class="mobile__footer">
                <div class="mobile__logo">
                    <img src="/local/templates/rospatriot/static/img/general/mobile-logo.png" alt="">
                </div>
            </div>
        </div>
        <header class="header">
            <div class="container-fluid">
                <div class="header__section">
                    <a href="/" class="header__logo">
                        <img src="/local/templates/rospatriot/static/img/general/logo1.png" alt="">
                    </a>
                </div>
                <div class="header__section header__section-rg">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top",
                        [
                            "ROOT_MENU_TYPE" => "top",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => [],
                            "MAX_LEVEL" => 2,
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ],
                        false
                    ); ?>
                    <div class="header__search">
						<?$APPLICATION->IncludeComponent(
							"bitrix:search.form",
							"header",
							Array(
								"PAGE" => "#SITE_DIR#search/index.php",
								"USE_SUGGEST" => "N"
							)
						);?>
                	</div>

                    <div class="header__end">
                        <a href="https://fadm.gov.ru/" target="_blank" class="header__sublogo">
                            <img src="/local/templates/rospatriot/static/img/general/rm.svg" alt="">
                        </a>
                    </div>
                    <div class="header__hamburger">
                        <div class="header__line"></div>
                        <div class="header__line"></div>
                        <div class="header__line"></div>
                    </div>
                </div>
            </div>
        </header>
        <?php
        // если кнопка "Показать включаемые области" нажата, то добавляем пустое место
        if ($APPLICATION->GetShowIncludeAreas()):?>
        <DIV style="height:120px;clear:both">&nbsp;</DIV>
        <?php endif;?>
        <!-- end::header -->
        <?=  defined('IN_CONTENT') ? '' : '</div><!-- /.document_content -->'; // tag must be closed for inner pages
        ?>
