<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "РОСПАТРИОТЦЕНТР - Контакты пресс службы");
$APPLICATION->SetTitle("Контакты Пресс-службы");
?>
<div class="corporate">
    <div class="container">
        <div class="page-title">
            Контакты пресс-службы
        </div>

        <div class="container min-bg-white">
            <div class="contact__address">
                <div class="contact__address-title">
                    Для обращений СМИ, связанных с освещением деятельности Роспатриотцентра
                </div>
                <div class="contact__address-list">
                    <div class="contact__address-item first">
                        <span class="icon"><svg class="icon__phone-fill" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#phone-fill">
                                </use>
                            </svg></span>
                        <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/corporate/phone.php"
	)
);?>
                    </div>
                    <div class="contact__address-item second">
                        <span class="icon"><svg class="icon__mail-fill" width="24px" height="24px">
                                <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#mail-fill">
                                </use>
                            </svg></span>
                        <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/corporate/email.php"
	)
);?>
                    </div>
                </div>
                <div class="corporate__text">
                    <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/corporate/text.php"
	)
);?>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>