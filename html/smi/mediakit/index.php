<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "РОСПАТРИОТЦЕНТР - Фирменный стиль");
$APPLICATION->SetTitle("Фирменный стиль");
?>
<div class="identity">
    <div class="container">
        <div class="title-page">
            Фирменный стиль
        </div>
        <div class="identity__banner">
            <div class="identity__banner-content">
                <div>
                    Загрузить нужные вам материалы и ознакомиться с правилами их размещения
                </div>
                <a href="/local/templates/rospatriot/static/pdf/rospatriot_bb.pdf" class="btn-base btn">Загрузить руководство</a>
            </div>
            <div class="identity__banner-img">
                <img src="/local/templates/rospatriot/static/img/general/identity-img.png">
            </div>
        </div>
        <div class="identity__logo">
            <div class="h4">
                Логотип
            </div>
            <p>
                Фирменный логотип включает в себя знак, сокращенное название организации и слоган. Элементы логотипа
                могут использоваться отдельно друг от друга, при этом начертание слогана в самостоятельном варианте
                меняется.
            </p>
            <p>
                Логотип отражает деятельность организации, нацеленность на результат и активные действия, которые
                создают наше будущее.
            </p>
            <div class="identity__logo-list">
                <div class="identity__logo-item">
                    <div class="identity__logo-wrap">
                        <div class="identity__logo-image">
                            <img src="/local/templates/rospatriot/static/img/general/Logo-img.png">
                        </div>
                        <div class="identity__logo-file">
                            <input type="file" id="file"> <label for="file">Загрузить логотип</label>
                        </div>
                    </div>
                    Основная версия
                </div>
                <div class="identity__logo-item">
                    <div class="identity__logo-wrap">
                        <div class="identity__logo-image">
                            <img src="/local/templates/rospatriot/static/img/general/Logo_monochrome.png">
                        </div>
                        <div class="identity__logo-file">
                            <input type="file" id="file"> <label for="file">Загрузить логотип</label>
                        </div>
                    </div>
                    Монохромная версия
                </div>
                <div class="identity__logo-item">
                    <div class="identity__logo-wrap">
                        <div class="identity__logo-image">
                            <img src="/local/templates/rospatriot/static/img/general/Logo_eng.png">
                        </div>
                        <div class="identity__logo-file">
                            <input type="file" id="file"> <label for="file">Загрузить логотип</label>
                        </div>
                    </div>
                    English version
                </div>
            </div>
        </div>
        <div class="identity__color">
            <div class="h4">
                Основные цвета
            </div>
            <p>
                Основные цвета фирменного стиля — красный, бордовый, черный и белый. Обращаем внимание, что на слайде
                представлены фирменные оттенки в разных системах (для электронных носителей - RGB, для печати - CMYK,
                для оформления пространства - РАNТОNЕ, для веб-разработки - HЕХ).
            </p>
            <div class="identity__color-list">
                <div class="identity__color-item red-light-block">
                    <div class="wrap-content">
                        <div class="color-settings">
                            RGB204 0 1
                        </div>
                        <div class="color-settings">
                            CMYK0 7 100 3
                        </div>
                        <div class="color-settings">
                            HEXCC0001
                        </div>
                        <div class="color-settings">
                            pantone2035
                        </div>
                    </div>
                </div>
                <div class="identity__color-item red-block">
                    <div class="wrap-content">
                        <div class="color-settings">
                            RGB204 0 1
                        </div>
                        <div class="color-settings">
                            CMYK0 7 100 3
                        </div>
                        <div class="color-settings">
                            HEXCC0001
                        </div>
                        <div class="color-settings">
                            pantone2035
                        </div>
                    </div>
                </div>
                <div class="identity__color-item black-block">
                    <div class="wrap-content">
                        <div class="color-settings">
                            RGB0 0 0
                        </div>
                        <div class="color-settings">
                            CMYK0 0 0 100
                        </div>
                        <div class="color-settings">
                            HEX000000
                        </div>
                    </div>
                </div>
                <div class="identity__color-item white-block">
                    <div class="wrap-content">
                        <div class="color-settings">
                            RGB255 255 255
                        </div>
                        <div class="color-settings">
                            CMYK0 0 0 0
                        </div>
                        <div class="color-settings">
                            HEXFFFFFF
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="identity__text">
            <div class="identity__text-info">
                <div class="h4">
                    Фирменный шрифт
                </div>
                <p>
                    В качестве фирменного шрифта используется Source Sans Pro. Обращаем внимание, что для наборных
                    текстов следует использовать начертания Regular и Light, а для заголовков - Bold и Black. Начертание
                    Semibold подойдет для выделений в тексте и подзаголовков.
                </p>
            </div>
            <div class="identity__text-name">
                Source Sans Pro
            </div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>