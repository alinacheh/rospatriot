<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Роспатриотцентр — Федеральное государственное бюджетное учреждение «Российский центр гражданского и патриотического воспитания детей и молодёжи» (сокр. ФГБУ «Роспатриотцентр», «Роспатриотцентр»). Создан Министерством образования Российской Федерации в 2002 году.");
$APPLICATION->SetPageProperty("title", "РОСПАТРИОТЦЕНТР - О нас");
$APPLICATION->SetTitle("О нас");
?>
<div class="document__content">
    <div class="header">
    </div>
    <div class="about">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="about__info">
                        <div class="about__title">
                            О нас
                        </div>
                        <div class="about__text">
                            Роспатриотцентр на протяжении пяти последних лет занимается комплексным развитием и
                            сопровождением всех видов деятельности по патриотическому воспитанию и популяризации
                            волонтерства в России.
                        </div>
                        <div class="about__text">
                            Федеральное государственное бюджетное учреждение «Российский центр гражданского и
                            патриотического воспитания детей и молодежи» (Роспатриотцентр) является подведомственным
                            учреждением Федерального агентства по делам молодежи.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-full">
            <div class="rospatriot-icon">
            </div>
            <div class="about__video">
                <div class="about__video-play" id="about-video-play">
                </div>
                <video controls="controls" onplay="onPlayAbout();" onpause="onPauseAbout();" id="about-video">
                    <source src="/local/templates/rospatriot/static/img/general/video.mp4" type="video/mp4">
                    Тег video не поддерживается вашим браузером. <a
                        href="/local/templates/rospatriot/static/img/general/video.mp4">Скачайте видео</a>. </video>
            </div>
        </div>
        <div class="container">
            <div class="about__info2">
                <div class="about__info2-line">
                    <img src="/local/templates/rospatriot/static/img/general/gline.png" alt="">
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="big">
                            Мы
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="about__info2-item">
                            оператор Государственной программы «Патриотическое воспитание граждан Российской Федерации
                            на 2016-2020 годы»;
                        </div>
                        <div class="about__info2-item">
                            координатор Федерального проекта «Социальная активность» Национального проекта
                            «Образование».
                        </div>
                    </div>
                </div>
                <div class="about__info2-line">
                    <img src="/local/templates/rospatriot/static/img/general/gline.png" alt="">
                </div>
            </div>
            <div class="about__mission">
                <div class="about__mission-item first">
                    <div class="about__mission-title">
                        Миссия
                    </div>
                    <p>
                        Делать для будущего, объединяя актуальные интересы общества и государства!
                    </p>
                </div>
                <div class="about__mission-item second">
                    <div class="about__mission-title">
                        Цель
                    </div>
                    <p>
                        Сформировать у молодежи адекватное отношение к понятию «патриотизм», к гражданской активности и
                        личной ответственности, сделать волонтерство неотъемлемой частью культуры общества.
                    </p>
                </div>
            </div>
        </div>
        <div class="container-full">
            <div class="about__activity">
                <div class="about__activity-title">
                    Направления деятельности
                </div>
                <div class="about__activity-wrap">
                    <div class="about__activity-banner right">
                        <img src="/local/templates/rospatriot/static/img/general/about1.png" alt="">
                    </div>
                    <div class="about__activity-item left">
                        <div class="about__activity-logo">
                            <img src="/local/templates/rospatriot/static/img/general/team1.png" alt="">
                        </div>
                        <div class="about__activity-text">
                            <div class="about__activity-team">
                                добро в россии
                            </div>
                            <div class="about__activity-subtitle">
                                Вовлечение молодежи в волонтерскую деятельность
                            </div>
                            <div>
                                Организовываем массовые мероприятия и образовательные проекты для волонтеров,
                                поддерживаем и тиражируем лучшие добровольческие инициативы, популяризируем
                                волонтерство.
                            </div>
                            <a href="/social-activity/" class="read-more purple">перейти в раздел</a>
                        </div>
                    </div>
                </div>
                <div class="about__activity-line">
                    <img src="/local/templates/rospatriot/static/img/general/gline.png" alt="">
                </div>
                <div class="about__activity-wrap">
                    <div class="about__activity-banner left">
                        <img src="/local/templates/rospatriot/static/img/general/about2.png" alt="">
                    </div>
                    <div class="about__activity-item right">
                        <div class="about__activity-logo">
                            <img src="/local/templates/rospatriot/static/img/general/team2.png" alt="">
                        </div>
                        <div class="about__activity-text">
                            <div class="about__activity-team">
                                Недиванный патриот
                            </div>
                            <div class="about__activity-subtitle">
                                Патриотическое воспитание молодежи
                            </div>
                            <div>
                                Поддерживаем патриотические проекты, проводим конкурсы и встречи с Героями России,
                                помогаем организовывать поисковые работы и исторические реконструкции.
                            </div>
                            <a href="/patriotic/" class="read-more red">перейти в раздел</a>
                        </div>
                    </div>
                </div>
                <div class="about__activity-line">
                    <img src="/local/templates/rospatriot/static/img/general/gline.png" alt="">
                </div>
                <div class="about__activity-wrap">
                    <div class="about__activity-banner right">
                        <img src="/local/templates/rospatriot/static/img/general/about3.png" alt="">
                    </div>
                    <div class="about__activity-item left">
                        <div class="about__activity-logo">
                            <img src="/local/templates/rospatriot/static/img/general/team3.png" alt="">
                        </div>
                        <div class="about__activity-text">
                            <div class="about__activity-team">
                                Future Team | Команда Будущего
                            </div>
                            <div class="about__activity-subtitle">
                                Вовлечение зарубежной молодежи в российские проекты и мероприятия для обмена опытом
                            </div>
                            <div>
                                Обеспечиваем взаимодействие профессиональной молодежи из России и зарубежных стран,
                                налаживание партнерских отношений и тиражирование эффективных социальных практик.
                            </div>
                            <a href="javascript: void(0)" class="read-more purple" data-toggle="modal" data-target="#feat">подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="overlay" class="hidden">
</div>
<div class="modal become" id="feat" role="dialog" aria-labelledby="thanks" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
				<h3>Future Team | Команда будущего</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <svg class="icon__close" width="47.971px" height="47.971px">
                        <use xlink:href="/local/templates/rospatriot/static/img/general/sprite.svg#close"></use>
                    </svg>

                </button>
            </div>
            <div class="modal-body">
                <div class="popup__text">
                    <p class="popup__text-bold">
                        Вовлечение зарубежной молодёжи в российские проекты и мероприятия для обмена опытом
                    </p>
                    <p>
                        Роспатриотцентр координирует деятельность международного движения Future Team.
                    </p>
                    <p>
                        Международное движение Future Team — сообщество активной, неравнодушной, стремящейся к
                        изменениям
                        молодёжи со всех уголков планеты. Оно было создано по инициативе участников XIX Всемирного
                        фестиваля
                        молодёжи и студентов.
                    </p>
                </div>
                <div class="popup__images">
                    <div class="popup__images-item">
                        <div class="popup__images-img">
                            <img src="/local/templates/rospatriot/static/img/general/futureteam/fufure_team_1.jpg" alt="">
                        </div>
                        <p class="popup__text-bold">
                            Первая постфестивальная встреча на форуме «Россия — страна возможностей»
                        </p>
                    </div>
                    <div class="popup__images-item">
                        <div class="popup__images-img">
                            <img src="/local/templates/rospatriot/static/img/general/futureteam/fufure_team_2.jpg" alt="">
                        </div>
                        <p class="popup__text-bold">
                            Делегация Future Team на Санкт-Петербургском международном экономическом форуме
                        </p>
                    </div>
                    <div class="popup__images-item">
                        <div class="popup__images-img">
                            <img src="/local/templates/rospatriot/static/img/general/futureteam/fufure_team_3.jpg" alt="">
                        </div>
                        <p class="popup__text-bold">
                            Медиаэкспедиция для молодых иностранных блогеров по городам-организаторам ЧМ-2018
                        </p>
                    </div>
                    <div class="popup__images-item">
                        <div class="popup__images__img">
                            <img src="/local/templates/rospatriot/static/img/general/futureteam/fufure_team_4.jpg" alt="">
                        </div>
                        <p class="popup__text-bold">
                            Участие иностранных волонтеров-участников движения в Международном форуме добровольцев
                        </p>
                    </div>
                </div>
                <button class="popup__btn" style="position: static; margin-left: auto; margin-right: auto;">futureteam.world</button>
            </div>
        </div>
    </div>
</div>



<div id="popup" class="hidden popup">
    <div class="popup__body popup__open">

    </div>
</div>
<script type="text/javascript">
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>